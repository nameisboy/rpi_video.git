<?php

class DBManager{
    var $servername = "localhost";
    var $username   = "root";
    var $password   = "123456";
    var $dbname     = "VideoRemoteCtrl";
    var $dev_info   = "dev_info";
    
    /**
     * 更新设备上线信息
     */
    function UpdateDevOnlineInfo($net_id, $dev_id, $video_addr, $ip, $status){
        // 创建连接
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // 检测连接
        if ($conn->connect_error) {
            die("连接失败: " . $conn->connect_error);
        }
        $select_sql = "SELECT * FROM $this->dev_info WHERE DevId='$dev_id'";
        $result = $conn->query($select_sql);
        if ($result->num_rows > 0) {
            echo "$dev_id already store in DB.\n";
            $update_sql = "UPDATE $this->dev_info
                SET NetID='$net_id', VideoAddr='$video_addr', IP='$ip', STATUS='$status'
                WHERE DevId=$dev_id";
            if ($conn->query($update_sql) === TRUE) {
                $rows = mysqli_affected_rows($conn);
                if($rows <= 0){
                    echo "update dev_info fail, maybe $net_id isn't a dev.\n";
                }else{
                    echo "update dev_info success, affected rows: $rows\n";
                }
            } else {
                echo "Error: " . $update_sql . "<br>" . $conn->error;
            }
        } else {
            echo "$dev_id did not store in DB.\n";
            $insert_sql = "INSERT INTO $this->dev_info (NetID, DevID, VideoAddr, IP, STATUS) 
                            VALUES 
                            ('$net_id', '$dev_id', '$video_addr', '$ip', '$status')";
            if ($conn->query($insert_sql) === TRUE) {
                echo "New record insert success.\n";
            } else {
                echo "Error: " . $insert_sql . "<br>" . $conn->error;
            }
        }

        // 关闭
        $conn->close();
    }
    /**
     * 更新设备下线信息
     */
    function UpdateDevOfflineInfo($net_id, $status){
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // 检测连接
        if ($conn->connect_error) {
            die("连接失败: " . $conn->connect_error);
        }
        $update_sql = "UPDATE $this->dev_info 
                        SET NetID='0', VideoAddr='0', IP='0', STATUS='$status'
                        WHERE NetID='$net_id'";
        if ($conn->query($update_sql) === TRUE) {
            $rows = mysqli_affected_rows($conn);
            if($rows <= 0){
                echo "update dev_info fail, maybe $net_id isn't a dev.\n";
            }else{
                echo "update dev_info success, affected rows: $rows\n";
            }
        } else {
            echo "Error: " . $update_sql . "<br>" . $conn->error;
        }

    }
    function GetNetIdByDevid($dev_id){
        // 创建连接
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // 检测连接
        if ($conn->connect_error) {
            die("连接失败: " . $conn->connect_error);
        }
        $select_sql = "SELECT * FROM $this->dev_info WHERE DevId=$dev_id";
        $result = $conn->query($select_sql);
        if ($result->num_rows > 0) {
            // 输出数据
            while($row = $result->fetch_assoc()) {
                return $row["NetID"];
            }
        }
    }
    /**
     * 获取设备状态(离线、或在线)
     */
    function GetDevStatusByDevid($dev_id){
        // 创建连接
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // 检测连接
        if ($conn->connect_error) {
            die("连接失败: " . $conn->connect_error);
        }
        $select_sql = "SELECT `STATUS` FROM $this->dev_info WHERE DevId=$dev_id";
        $result = $conn->query($select_sql);
        if ($result->num_rows > 0) {
            // 输出数据
            while($row = $result->fetch_assoc()) {
                return $row["STATUS"];
            }
        }
    }
}

// $db = new DBManager();
// $db->UpdateDevOnlineInfo("7f0000010b5700000001", "10000001", "xxx", "127.0.0.1", "online");
// $db->UpdateDevOfflineInfo("7f0000010b5700000001", "offline");
// $net_id = $db->GetNetIdByDevid("10000001");
// echo "net_id: $net_id\r\n"
// $status = $db->GetDevStatusByDevid("10000001");
// echo "status: $status\r\n";
?>