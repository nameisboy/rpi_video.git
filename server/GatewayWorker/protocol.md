## 系统通信流程
### 1、设备上线
online#dev_id#video_addr#
### 2、设备下线
若设备正常下线，在关闭回调中判断
异常断开下线，检测心跳异常后更新设备连接状态
### 3、下发控制
control#dev_id#left#


## 数据库设计
### 数据库：VideoRemoteCtrl
### 数据表
- app_info
	记录app客户端信息
	- NetID: 网络id，用于通信
	- IP: 客户端IP地址
	- STATUS: 状态，保留使用
- dev_info
	记录设备信息
	- NetID: 网络id，用于通信 
	- DevID: 设备id，用于辨识设备 
	- VideoAddr: 视频流地址、设备推流地址、拉流端使用该地址拉流
	- IP: 设备IP地址
	- STATUS: 状态
		- 在线：online
		- 离线: offline
	
