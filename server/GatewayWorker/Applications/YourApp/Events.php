<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;
include("mysql/mysql_handle.php");
/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据 
        // Gateway::sendToClient($client_id, "Hello $client_id\r\n");
        $datetime = new DateTime();
        $time = $datetime->format('Y-m-d H:i:s');
        $ip = $_SERVER['REMOTE_ADDR'];
        echo "online msg: [$client_id]($ip) login at $time\n";
        // 向所有人发送
        // Gateway::sendToAll("$client_id login\r\n");
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message)
   {
        // Gateway::sendToAll("$client_id said $message\r\n");
        $remote_ip = $_SERVER['REMOTE_ADDR'];//远端ip
        $data_array = explode('#', $message);
        $len = count($data_array);
        if($len <= 0){
            echo "message data error, data: [$message]\n";
            return;
        }
        $db = new DBManager();
        switch($data_array[0]){//处理
            case "online":
                $db->UpdateDevOnlineInfo($client_id, $data_array[1], $data_array[2], $remote_ip, "online");
                break;
            case "control":
                $id = $data_array[1];
                if("online" != $db->GetDevStatusByDevid($id)){//设备离线
                    echo "dev($id) is not online.\n";
                    break;
                }else{
                    // 1.获取网络id
                    $net_id = $db->GetNetIdByDevid($id);
                    echo "ctrl dev's net_id: $net_id\n";
                    // 2.再次检查是否在线
                    if(1 == Gateway::isOnline($net_id)){
                        // 3.发送数据给设备
                        Gateway::sendToClient($net_id, "#$data_array[2]#\r\n");
                    }else{
                        echo "dev: $id isn't online.\n";
                    }
                }

                break;
        }
    }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {
        // 向所有人发送 
        //    GateWay::sendToAll("$client_id logout\r\n");
        $datetime = new DateTime();
        $time = $datetime->format('Y-m-d H:i:s');
        echo "offline: $client_id logout at $time\n";
        $db = new DBManager();
        $db->UpdateDevOfflineInfo($client_id, "offline");
   }
}

