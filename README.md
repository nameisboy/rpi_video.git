# rpi_video
基于树莓派3B实现的视频监控小项目，包含：
- 固件
- app
- 服务器
    - nginx rtmp-server服务器(百度安装，添加支持H264)
    - GatewayWorker TCP长连接服务器
        - 简单通信协议：server/GatewayWorker/protocol.md

更详细的介绍：[树莓派视频监控](https://blog.csdn.net/qq_41790078/category_12066082.html?spm=1001.2014.3001.5482)
