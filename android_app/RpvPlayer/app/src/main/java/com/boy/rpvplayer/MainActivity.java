package com.boy.rpvplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.kongqw.rockerlibrary.view.RockerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import cn.nodemedia.NodePlayer;
import cn.nodemedia.NodePlayerDelegate;
import cn.nodemedia.NodePlayerView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NodePlayerDelegate {
    private static final String server_ip = "114.132.79.49";
    private static final int server_port = 8080;
    private Socket sock_client;
    private BufferedReader sock_in = null;
    private PrintWriter sock_out = null;
    private String sock_content = "";
    private Handler handler_sock = null;

    private Button btnPlay;//播放按钮
    private Button btnCtrlUp,btnCtrlLeft,btnCtrlDown,btnCtrlRight;//控制按钮
    private NodePlayerView npvPlay;//播放器控件
    private NodePlayer nodePlayer;//播放器对象
    private String rtmpUrl = "rtmp://120.79.54.142:1935/live/rpi";
//    private String rtmpUrl = "http://zhibo.hkstv.tv/livestream/mutfysrq/playlist.m3u8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();//绑定控件
        SocketThreadInit();
        SocketHandlerInit();
    }

    //处理socket的handler
    private void SocketHandlerInit() {
        handler_sock = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                switch (msg.what){
                    case 0x01:
                        break;
                    case 0x02:
                        break;
                    case 0x03:
                        break;
                    default:
                        Log.i("sock_content", "Unkown msg: "+msg.obj.toString());
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        nodePlayer.stop();
        nodePlayer.release();
        try {
            sock_client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void bindViews(){
        btnPlay = findViewById(R.id.operate_btn);
        btnPlay.setOnClickListener(this);

        npvPlay = findViewById(R.id.rtmpNpv);
        npvPlay.setRenderType(NodePlayerView.RenderType.SURFACEVIEW);
        npvPlay.setUIViewContentMode(NodePlayerView.UIViewContentMode.ScaleAspectFit);

        nodePlayer = new NodePlayer(this);
        nodePlayer.setPlayerView(npvPlay);
        nodePlayer.setNodePlayerDelegate(this);
        nodePlayer.setHWEnable(true);

        btnCtrlUp = findViewById(R.id.btn_ctrl_up);
        btnCtrlUp.setOnClickListener(this);

        btnCtrlRight = findViewById(R.id.btn_ctrl_right);
        btnCtrlRight.setOnClickListener(this);

        btnCtrlLeft = findViewById(R.id.btn_ctrl_left);
        btnCtrlLeft.setOnClickListener(this);

        btnCtrlDown = findViewById(R.id.btn_ctrl_down);
        btnCtrlDown.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.operate_btn:
                String btn_text = btnPlay.getText().toString();
                Log.i("onClick", "btn_text:" + btn_text);
                if(btn_text.equals(this.getString(R.string.rtmp_play))){
                    Log.i("onClick", "start play-video");
                    if(!nodePlayer.isPlaying()){
                        nodePlayer.setInputUrl(rtmpUrl);
                        nodePlayer.start();
                    }
                    btnPlay.setText(R.string.rtmp_stop);
                }else if(btn_text.equals(this.getString(R.string.rtmp_stop))){
                    Log.i("onClick", "stop play-video");
                    nodePlayer.stop();
                    btnPlay.setText(R.string.rtmp_play);
                }
                break;
            case R.id.btn_ctrl_up:
                Toast.makeText(MainActivity.this, "up", Toast.LENGTH_SHORT).show();
                SocketSendMsg("control#10000001#up#");
                break;
            case R.id.btn_ctrl_left:
                Toast.makeText(MainActivity.this, "left", Toast.LENGTH_SHORT).show();
                SocketSendMsg("control#10000001#left#");
                break;
            case R.id.btn_ctrl_right:
                Toast.makeText(MainActivity.this, "right", Toast.LENGTH_SHORT).show();
                SocketSendMsg("control#10000001#right#");
                break;
            case R.id.btn_ctrl_down:
                Toast.makeText(MainActivity.this, "down", Toast.LENGTH_SHORT).show();
                SocketSendMsg("control#10000001#down#");
                break;
        }
    }

    //  socket线程初始化，连接服务器
    private void SocketThreadInit(){
        new Thread(){
            @Override
            public void run() {
                try {
                    sock_client = new Socket(server_ip, server_port);//创建socket并连接服务器
                    sock_in = new BufferedReader(new InputStreamReader(sock_client.getInputStream(), "UTF-8"));
                    sock_out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                            sock_client.getOutputStream())), true);
                    while(true){
                        if(!sock_client.isInputShutdown()){//输入流未关闭
                            try {
                                if((sock_content = sock_in.readLine()) != null){//有内容可读
                                    Log.i("sock_content", "sock read: "+sock_content.toString());
                                    Message msg = Message.obtain();
                                    msg.obj = sock_content;
                                    msg.what=0x00;   //标志消息的标志
                                    handler_sock.sendMessage(msg);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void SocketSendMsg(final String data){
        new Thread(){
            @Override
            public void run() {
                if (sock_client.isConnected()) {
                    if (!sock_client.isOutputShutdown()) {
                        Log.i("sock_content", "snd-data: "+data);
                        sock_out.println(data);
                    }
                }
            }
        }.start();
    }

    /**
     * NodePlayer事件触发回调
     * @param player
     * @param event
     * @param msg
     */
    @Override
    public void onEventCallback(NodePlayer player, int event, String msg) {
        switch(event){
            case 1000:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "连接中", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case 1001:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "连接成功", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case 1002:
                // 视频连接失败 流地址不存在，或者本地网络无法和服务端通信，回调这里。5秒后重连， 可停止
                Log.e("error", msg);
                break;
            case 1003:
                // 视频开始重连,自动重连总开关
    //                nodePlayer.stopPlay();
                Log.e("error", msg);
                break;
            case 1004:
                // 视频播放结束
                Log.e("error", msg);
                break;
            case 1005:
                // 网络异常,播放中断,播放中途网络异常，回调这里。1秒后重连，如不需要，可停止
                Log.e("error", msg);
                break;
            case 1006:
                //RTMP连接播放超时
                Log.e("error", msg);
                break;
            case 1100:
                // 播放缓冲区为空
                Log.e("error", msg);
                break;
            case 1101:
                // 播放缓冲区正在缓冲数据,但还没达到设定的bufferTime时长
                Log.e("error", msg);
                break;
            case 1102:
                // 播放缓冲区达到bufferTime时长,开始播放.
                // 如果视频关键帧间隔比bufferTime长,并且服务端没有在缓冲区间内返回视频关键帧,会先开始播放音频.直到视频关键帧到来开始显示画面.
                Log.e("error", msg);
                break;
            case 1103:
//				System.out.println("Stream EOF");
                // 客户端明确收到服务端发送来的 StreamEOF 和 NetStream.Play.UnpublishNotify时回调这里
                // 注意:不是所有云cdn会发送该指令,使用前请先测试
                // 收到本事件，说明：此流的发布者明确停止了发布，或者因其网络异常，被服务端明确关闭了流
                // 本sdk仍然会继续在1秒后重连，如不需要，可停止
                Log.e("error", msg);
                break;
            case 1104:
                //解码后得到的视频高宽值 格式为:{width}x{height}
                Log.e("error", msg);
                break;
            default:
                break;
        }
    }
}
