#include "cipc.h"


// 内部函数声明
static int CheckShmByKey(key_t shm_key);

/**
 * @brief 初始化一块共享内存
 * 
 * @param shm_key 共享内存对应的KEY值
 * @param size 大小
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t InitShm(key_t shm_key, size_t size)
{
    int shm_id = -1;
    shm_id = shmget(shm_key, size, 0777 | IPC_CREAT);//创建共享内存，权限为0666
    if(0 > shm_id){
        perror("shmget()");
        return SHM_CREAT_FAIL;
    }

    return SHM_OK;
}
/**
 * @brief 写数据到共享内存
 * 
 * @param shm_key 需要写入的共享内存对应的key
 * @param p_data_w 写入数据
 * @param w_size 写入数据大小，单位：字节
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t WriteToShm(key_t shm_key, const void *p_data_w, unsigned int w_size)
{
    int shm_id = -1;
    if(0 > (shm_id = CheckShmByKey(shm_key))){
        return SHM_NOT_EXIST;
    }
    void *p_addr_w = shmat(shm_id, NULL, 0);//读写模式映射共享内存到进程空间
    memcpy(p_addr_w, p_data_w, w_size);
    if(0 != shmdt(p_addr_w)){
        perror("shmdt()");
        return SHM_DT_FAIL;
    }
    return SHM_OK;
}
/**
 * @brief 从共享内存读取数据
 * 
 * @param shm_key 需要读取的共享内存所对应的key
 * @param p_r_data 读取后的数据存放区
 * @param r_size 读取的大小，单位：字节
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t ReadFromShm(key_t shm_key, void *p_r_data, unsigned int r_size)
{
    int shm_id = -1;
    if(0 > (shm_id = CheckShmByKey(shm_key))){
        return SHM_NOT_EXIST;
    }
    void *p_addr_r = shmat(shm_id, NULL, SHM_RDONLY);//读取共享内存时使用只读方式映射
    memcpy(p_r_data, p_addr_r, r_size);
    if(0 != shmdt(p_addr_r)){
        perror("shmdt()");
        return SHM_DT_FAIL;
    }

    return  SHM_OK;
}
/**
 * @brief 删除系统中的共享内存
 * 
 * @param shm_key 需要删除的共享内存所对应的key
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t DeleteShm(key_t shm_key)
{
    int shm_id = -1;
    if(0 > (shm_id = CheckShmByKey(shm_key))){
        return SHM_NOT_EXIST;
    } 
    if(0 != shmctl(shm_id, IPC_RMID, NULL)){
        perror("shmctl()-IPC_RMID");
        return SHM_DEL_FAIL;
    }

    return SHM_OK;
}

/**
 * @brief 检查共享内存是否存在于系统中
 * 
 * @param shm_key 需要检查的共享内存的key
 * @return int 存在，返回共享内存对于的id，否则返回小于0的错误。 
 */
static int CheckShmByKey(key_t shm_key)
{
    int shm_id = -1;
    shm_id = shmget(shm_key, 0, 0666);
    if(0 > shm_id){
        perror("shmget()");
        if(ENOENT == errno){
            printf("No the shm of key(%#x) in this system!\n", shm_key);
        }
        return SHM_NOT_EXIST;
    }
    return shm_id;
}