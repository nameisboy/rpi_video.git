#include "cipc.h"

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short  *array;
    struct seminfo  *__buf;
};

// 内部函数声明
static int CheckSemByKey(key_t sem_key);

/**
 * @brief 初始化二值信号量
 * 
 * @param sem_key 信号量的key
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t InitBinarySem(key_t sem_key)
{
    union semun sem_union;
    int sem_id = -1;
    if(0 > (sem_id = semget(sem_key, 1, 0777 | IPC_CREAT))){//二值信号量，数目设置为1
        perror("semget()");
        return SEM_CREAT_FAIL;
    }
    memset(&sem_union, 0, sizeof(sem_union));
    sem_union.val = 1; //初始值为0
    if(-1 == semctl(sem_id, 0, SETVAL, sem_union)){
        perror("semctl()-SETVAL");
        return SEM_SETVAL_FAIL;
    }
    return SEM_OK;
}

/**
 * @brief 信号量申请操作
 * 
 * @param sem_key 需要操作的信号量所对应的key
 * @param wait_flag 阻塞标志，BLOCK_WAIT：阻塞等待；NOBLOCK_WAIT: 为非阻塞等待
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t SemBinaryP(key_t sem_key, WaitStyle_t wait_flag)
{
    int sem_id = -1;
    if(0 > (sem_id = CheckSemByKey(sem_key))){
        printf("No the Sem(key=%#x) exist in system!\n", sem_key);
        return SEM_NOT_EXIST;
    }
    struct sembuf op_buf;
    memset(&op_buf, 0, sizeof(op_buf));
    op_buf.sem_num = 0;//二值信号量，编号为0
    op_buf.sem_op = -1;//P操作减1
    op_buf.sem_flg = IPC_NOWAIT | SEM_UNDO;//默认非阻塞、进程退出释放
    if(BLOCK_WAIT == wait_flag){
        op_buf.sem_flg &= ~(IPC_NOWAIT);//清除非阻塞标志
    }else if (NOBLOCK_WAIT == wait_flag){
        op_buf.sem_flg |= IPC_NOWAIT;
    }
    if(0 != semop(sem_id, &op_buf, 1)){
        perror("semop()-P");
        return SEM_P_FAIL;
    }

    return SEM_OK;
}
/**
 * @brief 信号量释放操作
 * 
 * @param sem_key 需要操作的信号量对应的key
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t SemBinaryV(key_t sem_key)
{
    int sem_id = -1;
    if(0 > (sem_id = CheckSemByKey(sem_key))){
        printf("No the Sem(key=%#x) exist in system!\n", sem_key);
        return SEM_NOT_EXIST;
    }
    struct sembuf op_buf;
    op_buf.sem_num = 0;//二值信号量，编号为0
    op_buf.sem_op = +1;//V操作加1
    op_buf.sem_flg = IPC_NOWAIT | SEM_UNDO;//默认非阻塞、进程退出释放
    if(0 != semop(sem_id, &op_buf, 1)){
        perror("semop()-P");
        return SEM_V_FAIL;
    }
    return SEM_OK;
}
/**
 * @brief 删除系统中的信号量
 * 
 * @param sem_key 需要删除的信号量对应的key值
 * @return SemStatus_t 删除成功返回0，失败返回非0.
 */
SemStatus_t DeleteSem(key_t sem_key)
{
    int sem_id = -1;
    if(0 > (sem_id = CheckSemByKey(sem_key))){
        printf("No the Sem(key=%#x) exist in system!\n", sem_key);
        return SEM_NOT_EXIST;
    }
    union semun sem_union;
    if(0 != semctl(sem_id, 0, IPC_RMID, sem_union)){
        perror("semctl()-IPC_RMID");
        return SEM_DEL_FAIL;
    }

    return SEM_OK;
}
/**
 * @brief 通过key检查信号量是否存在于系统中
 * 
 * @param sem_key 信号量对应的key
 * @return int 成功，返回sem的id；否则，返回非0.
 */
static int CheckSemByKey(key_t sem_key)
{
    int sem_id = -1;
    if(0 > (sem_id = semget(sem_key, 0, 0666))){
        perror("semget()");
        if(ENOENT == errno){
            printf("No the sem of key(%#x) in this system!\n", sem_key);
        }
        return SEM_NOT_EXIST;
    }
    return sem_id;
}


//semtimedop //设置超时等待