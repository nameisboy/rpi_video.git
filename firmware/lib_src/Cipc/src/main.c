// #define DEBUG
#ifdef DEBUG

#include "cipc.h"
#include <string.h>
#include <sys/wait.h>

typedef struct 
{
    int  id;
    char name[20];
    unsigned char age;
}StudentObj_t;

#define SHM_KEY 0x0123125
#define SEM_KEY 0x0123126
int main(void)
{
// 1.共享内存部分
    StudentObj_t student;
    if(0 != InitShm(SHM_KEY, sizeof(student))){
        printf("InitShm fail!\n");
        exit(-1);
    }
    memset(&student, 0, sizeof(student));
    student.id = 101;
    strncpy(student.name, "Tom", 3);
    student.age = 18;
    WriteToShm(SHM_KEY, &student, sizeof(student));
    sleep(1);
    StudentObj_t student_info;
    memset(&student_info, 0, sizeof(student_info));
    if(0 != ReadFromShm(SHM_KEY, &student_info, sizeof(student_info))){
        printf("ReadFromShm fail!\n");
        exit(-1);
    }
    printf("id:\t\t %d\n", student_info.id);
    printf("name:\t\t %s\n", student_info.name);
    printf("age:\t\t %d\n", student_info.age);
    
    system("ipcs -m | grep '123125'\n");
    DeleteShm(SHM_KEY);
// 2.信号量部分
    if(0 != InitBinarySem(SEM_KEY)){
        printf("InitBinarySem err!\n");
        exit(-1);
    }
    pid_t pid = fork();
    if(0 == pid){//子进程
        printf("[%d]child-process start!\n", getpid());
        SemBinaryP(SEM_KEY, BLOCK_WAIT);
        printf("[%d]child-process SemBinaryP\n", getpid());
        for(int i=0; i<5; i++){
            sleep(1);
            printf("sleep(1)..\n");
        }
        SemBinaryV(SEM_KEY);
        printf("[%d]child-process SemBinaryV\n", getpid());
        exit(0);
    }else if(0 < pid){//父进程
        printf("[%d]parent-process start!\n", getpid());
        sleep(2);
        printf("[%d]parent-process SemBinaryP\n", getpid());
        SemBinaryP(SEM_KEY, BLOCK_WAIT);
        printf("special operation!\n");
        SemBinaryV(SEM_KEY);
        printf("[%d]parent-process SemBinaryV\n", getpid());
        int status = -1;
        pid_t c_pid = wait(&status);
        printf("cpid = [%d]\n", c_pid);
        DeleteSem(SEM_KEY);
        exit(0);
    }else{//错误
        perror("fork()");
        exit(-1);
    }

    printf("main process exit()!\n");
    exit(0);
}


#endif // DEBUG