#ifndef __CIPC_H
#define __CIPC_H
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <string.h>
	

#include <sys/sem.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

typedef enum{
    AUTO_MATCH = 0,//自动匹配
    FORCE_MATCH = 1,//强制匹配，系统中不存在该ipc对象，则创建
}MatchFlag_t;
/**
 * @brief 等待方式枚举
 * 
 */
typedef enum{
    BLOCK_WAIT      = 0,//阻塞等待
    NOBLOCK_WAIT    = 1,//非阻塞等待
}WaitStyle_t;
/**
 * @brief 共享内存相关枚举值
 * 
 */
typedef enum{
    SHM_DEL_FAIL   = -6,//删除shm失败
    SHM_DT_FAIL    = -5,//shmdt失败
    SHM_NOT_EXIST  = -4,//共享内存不存在
    SHM_RD_FAIL    = -3,//读失败
    SHM_WR_FAIL    = -2,//写失败
    SHM_CREAT_FAIL = -1,//创建失败
    SHM_OK         =  0,
}ShmStatus_t;

/**
 * @brief 信号量相关枚举值
 * 
 */
typedef enum{
    SEM_P_FAIL     = -9,//P操作失败
    SEM_V_FAIL     = -8,//V操作失败 
    SEM_SETVAL_FAIL= -7,//设值失败
    SEM_DEL_FAIL   = -6,//删除shm失败
    SEM_DT_FAIL    = -5,//shmdt失败
    SEM_NOT_EXIST  = -4,//共享内存不存在
    SEM_RD_FAIL    = -3,//读失败
    SEM_WR_FAIL    = -2,//写失败
    SEM_CREAT_FAIL = -1,//创建失败
    SEM_OK         =  0,
}SemStatus_t;
/**
 * @brief 初始化一块共享内存
 * 
 * @param shm_key 共享内存对应的KEY值
 * @param size 大小
 * @return int 返回0成功，其他值失败
 */
ShmStatus_t InitShm(key_t shm_key, size_t size);
/**
 * @brief 写数据到共享内存
 * 
 * @param shm_key 需要写入的共享内存对应的key
 * @param p_data_w 写入数据
 * @param w_size 写入数据大小，单位：字节
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t WriteToShm(key_t shm_key, const void *p_data_w, unsigned int w_size);
/**
 * @brief 从共享内存读取数据
 * 
 * @param shm_key 需要读取的共享内存所对应的key
 * @param p_r_data 读取后的数据存放区
 * @param r_size 读取到的大小
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t ReadFromShm(key_t shm_key, void *p_r_data, unsigned int r_size);
/**
 * @brief 删除系统中的共享内存
 * 
 * @param shm_key 需要删除的共享内存所对应的key
 * @return ShmStatus_t 返回0成功，其他值失败
 */
ShmStatus_t DeleteShm(key_t shm_key);
/**
 * @brief 初始化二值信号量
 * 
 * @param sem_key 信号量的key
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t InitBinarySem(key_t sem_key);
/**
 * @brief 信号量申请操作
 * 
 * @param sem_key 需要操作的信号量所对应的key
 * @param wait_flag 阻塞标志，BLOCK_WAIT：阻塞等待；NOBLOCK_WAIT: 为非阻塞等待
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t SemBinaryP(key_t sem_key, WaitStyle_t wait_flag);
/**
 * @brief 信号量释放操作
 * 
 * @param sem_key 需要操作的信号量对应的key
 * @return SemStatus_t 成功，返回0；否则返回非0错误
 */
SemStatus_t SemBinaryV(key_t sem_key);
/**
 * @brief 删除系统中的信号量
 * 
 * @param sem_key 需要删除的信号量对应的key值
 * @return SemStatus_t 删除成功返回0，失败返回非0.
 */
SemStatus_t DeleteSem(key_t sem_key);
#endif // IPC_SHM_H