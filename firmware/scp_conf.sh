#!/bin/sh

echo "diR: `pwd`"
remote_ip=`cat ip_addr`
echo "remote_ip: ${remote_ip}"
remote_path="/rpv/zlog/"
# check params
if [ $# -lt 1 ]
then
    echo "Usage: scp.sh <xxx>"
fi
# transform bin
sshpass -p "l" scp $* pi@${remote_ip}:${remote_path}
if test $? -eq 0
then
    echo "scp transform successful!\n"
    echo "\033[33m`md5sum $*`\033[0m"
else
    echo "scp transform fail!"
fi
