#!/bin/bash

# 设置环境变量，以确保为交叉编译工具链
export CC=/opt/rpi_tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc
export CXX=/opt/rpi_tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-g++
# 编译并上传
rm build -rf
mkdir -p build
cd build

cmake ..

if [ $# -eq 1 ] # 参数个数等于1
then
    make $1
    exit 0
fi
make all -j8
make install
# if [ $? -eq 0 ]
# then
#     cd -
#     ./scp.sh build/bin/*
#     ./scp_conf.sh common/conf/*
# fi

