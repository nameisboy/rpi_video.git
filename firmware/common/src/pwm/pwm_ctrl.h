#ifndef H_PWM_CTRL
#define H_PWM_CTRL
#include "rpv_com.h"


enum {
    NORMAL = 0,
    INVERSED = 1,
}PwmPolarityMode_t;
typedef struct{
    unsigned char pwm_chip_num;//第几个PWM控制器
    unsigned char pwm_channel_num;//PWM控制器第x输出通道
    unsigned int polarity;//输出极性
    unsigned int period;//PWM周期
    unsigned int duty_cycle;//PWM占空比
}PwmConfig_t;

// 外部接口函数
int PwmOpen(PwmConfig_t pwm);
int PwmSetDutyCycle(PwmConfig_t *pwm, double duty_cycle_percent);
#endif // !H_PWM_CTRL