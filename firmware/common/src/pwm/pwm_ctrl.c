#include "pwm_ctrl.h"

#define     PWM_FS                  "/sys/class/pwm/"
#define     PWM_EXPORT              "/sys/class/pwm/pwmchip%d/export"
#define     PWM_CHIPx_PWMx          "/sys/class/pwm/pwmchip%d/pwm%d"
#define     CHIPx_PWMx_ENABLE       "/sys/class/pwm/pwmchip%d/pwm%d/enable"
#define     CHIPx_PWMx_POLARITY     "/sys/class/pwm/pwmchip%d/pwm%d/polarity"
#define     CHIPx_PWMx_PERIOD       "/sys/class/pwm/pwmchip%d/pwm%d/period"
#define     CHIPx_PWMx_DUTY_CYCLE   "PWM_FS/sys/class/pwm/pwmchip%d/pwm%d/duty_cycle"

// #define     dzlog_error     printf
// #define     dzlog_notice    printf
/**
 * @brief 检查是否支持sysfs文件系统pwm子系统
 * 
 * @return int 
 *  返回0表示支持，否则不支持。
 */
static int IsSupportSysfsPwm(void)
{
    if(0 != access(PWM_FS, F_OK)){
        dzlog_error("[%s] is not exist.", PWM_FS);
        dzlog_error("Current system is not support Sysfs pwm.");
        return -1;
    }

    return 0;
}
/**
 * @brief 检查并导出pwm输出通道
 * 
 * @param channel 
 * @return int 
 */
static int CheckExportPwmChannel(PwmConfig_t pwm)
{
    int fd = -1;
    char pwm_path_buf[100];
    memset(pwm_path_buf, 0, sizeof(pwm_path_buf));
    snprintf(pwm_path_buf, sizeof(pwm_path_buf), PWM_CHIPx_PWMx, (int)pwm.pwm_chip_num, (int)pwm.pwm_channel_num);
    if(0 == access(pwm_path_buf, F_OK)){
        return 0;
    }
    dzlog_error("[%s] is not exist.\n", pwm_path_buf);
    dzlog_notice("Enable %s.\n", pwm_path_buf);
    do{
        memset(pwm_path_buf, 0, sizeof(pwm_path_buf));
        snprintf(pwm_path_buf, sizeof(pwm_path_buf), PWM_EXPORT, (int)pwm.pwm_chip_num);
        if(0 > (fd = open(pwm_path_buf, O_WRONLY))){
            dzlog_error("open [%s] failed.", pwm_path_buf);
            return -1;
        }
        char channel_str[10];
        memset(channel_str, 0, sizeof(channel_str));
        snprintf(channel_str, sizeof(channel_str), "%d", (int)pwm.pwm_channel_num);
        if(strlen(channel_str) != write(fd, channel_str, strlen(channel_str))){
            dzlog_error("write (%s) (%s) failed.", pwm_path_buf, channel_str);
            break;
        }
        close(fd);
        return 0;
    }while(0);

    close(fd);
    return -1;
}
/**
 * @brief 写pwm属性值
 * 
 * @param pwm 需要写入的pwm
 * @param attr 属性
 * @param val 属性值
 * @return int 返回0成功，否则失败。
 */
static int PwmConfig(int pwm_chip_num, int pwm_channel_num, const char *attr, const char *val)
{
    char file_path[100];
    int len;
    int fd;
    snprintf(file_path, sizeof(file_path), ""PWM_CHIPx_PWMx"/%s" , (int)pwm_chip_num, (int)pwm_channel_num, attr);
    if (0 > (fd = open(file_path, O_WRONLY))) {
        dzlog_error("open (%s) error: %s\r\n", file_path, strerror(errno));
        return -1;
    }
    len = strlen(val);
    if (len != write(fd, val, len)) {
        dzlog_error("write (%s) error: %s\r\n", val, strerror(errno));
        close(fd);
        return -1;
    }
    close(fd); //关闭文件
    return 0;
}
/**
 * @brief 打开Pwm通道
 * 
 * @param pwm 
 * @return int 返回0成功，否则失败。
 */
int PwmOpen(PwmConfig_t pwm)
{
    do{
        // 1.导出pwm
        if(0 != CheckExportPwmChannel(pwm)) break;
        // 2.配置极性
        // if(0 != PwmConfig(pwm.pwm_chip_num, pwm.pwm_channel_num, "polarity", ((pwm.polarity==NORMAL)?"normal":"inversed"))) break;
        // 3.配置周期
        char period_buf[32];
        memset(period_buf, 0, sizeof(period_buf));
        snprintf(period_buf, sizeof(period_buf), "%u", pwm.period);
        if(0 != PwmConfig(pwm.pwm_chip_num, pwm.pwm_channel_num, "period", period_buf)) break;
        // 4.配置占空比
        char duty_cycle_buf[32];
        memset(duty_cycle_buf, 0, sizeof(duty_cycle_buf));
        snprintf(duty_cycle_buf, sizeof(duty_cycle_buf), "%u", pwm.duty_cycle);
        if(0 != PwmConfig(pwm.pwm_chip_num, pwm.pwm_channel_num, "duty_cycle", duty_cycle_buf)) break;
        // 5.配置使能
        if(0 != PwmConfig(pwm.pwm_chip_num, pwm.pwm_channel_num, "enable", "1")) break;
        return 0;
    }while(0);

    dzlog_error("Open pwm channel failed, pwmchip:[%u], channel: [%u].", pwm.pwm_chip_num, pwm.pwm_channel_num);
    return -1;
}

int PwmRead(PwmConfig_t pwm, const char *attr, char *val)
{
    char file_path[100];
    int len;
    int fd;
    snprintf(file_path, sizeof(file_path), ""PWM_CHIPx_PWMx"/%s" , (int)pwm.pwm_chip_num, (int)pwm.pwm_channel_num, attr);
    if (0 > (fd = open(file_path, O_WRONLY))) {
        dzlog_error("open (%s) error: %s", file_path, strerror(errno));
        return fd;
    }
    len = strlen(val);
    if (len != read(fd, val, len)) {
        dzlog_error("read (%s) error: %s", val, strerror(errno));
        close(fd);
        return -1;
    }
    close(fd); //关闭文件
    return 0;
}


int PwmClose(PwmConfig_t pwm)
{
    
}

/**
 * @brief 设置pwm占空比
 * 
 * @param pwm 
 * @param duty_cycle_percent 需要设置的占空比(百分数，如: 100代表100%占空比)
 * @return int 
 */
int PwmSetDutyCycle(PwmConfig_t *pwm, double duty_cycle_percent)
{
    char duty_cycle_buf[32];
    memset(duty_cycle_buf, 0, sizeof(duty_cycle_buf));
    int duty_cycle = 0;
    duty_cycle = (duty_cycle_percent/100.0)*pwm->period;
    // printf("duty_cycle after-calcu: [%d].\n", duty_cycle);
    snprintf(duty_cycle_buf, sizeof(duty_cycle_buf), "%d", duty_cycle);
    if(0 != PwmConfig(pwm->pwm_chip_num, pwm->pwm_channel_num, "duty_cycle", duty_cycle_buf)){
        return -1;
    }
    pwm->duty_cycle = duty_cycle;
    return 0;
}