#include "rpv_com.h"

/**
 * @brief 初始化共用的IPC对象
 * 
 * @return int 
 */
int InitCommonIPC(void)
{
    // 1.初始化信号量
    while((SHM_OK != InitBinarySem(VIDEO_FRAME_SEM_KEY))){
		usleep(100*1000);
		printf("InitShm fail, shm key: [%#x]\n", VIDEO_FRAME_SEM_KEY);
	}	
	while((SHM_OK != InitBinarySem(VIDEO_PARAM_SEM_KEY))){
		usleep(100*1000);
		printf("InitShm fail, shm key: [%#x]\n", VIDEO_PARAM_SEM_KEY);
	}
    // 2.初始化共享内存
	while((SHM_OK != InitShm(VIDEO_FRAME_SHM_KEY, sizeof(VideoFrame_t)))){
		usleep(100*1000);
		printf("InitShm fail, shm key: [%#x]\n", VIDEO_FRAME_SEM_KEY);
	}
	while((SHM_OK != InitShm(VIDEO_PARAM_SHM_KEY, sizeof(CamParams_t)))){
		usleep(100*1000);
		printf("InitShm fail, shm key: [%#x]\n", VIDEO_PARAM_SHM_KEY);
	}

    return 0;
}