#ifndef RPV_COM_H
#define RPV_COM_H
#include "cipc.h"
#include "rpv_zlog.h"

/****************************IPC相关定义****************************/
//信号量
#define     VIDEO_FRAME_SEM_KEY     (0X111222) //视频帧信号锁，用于互斥访问
#define     VIDEO_PARAM_SEM_KEY     (0X111223) //视频参数信号锁，用于互斥访问
// 共享内存
#define     VIDEO_FRAME_SHM_KEY     (0X123123) //视频帧数据共享内存
#define     VIDEO_PARAM_SHM_KEY     (0X123124) //视频参数共享内存

#define     VIDEO_FRAME_SHM_SIZE    (1024*1024UL)// 视频帧共享内存大小


/**
 * @brief 摄像头参数结构体
 */
typedef struct __cam_params
{
    unsigned int pix_fmt;//像素格式
    unsigned int width;//帧宽
    unsigned int height;//帧高
    unsigned int fps;//帧率
    char stream_url[200];//推流时使用的地址
}CamParams_t;

typedef struct __video_frame
{
    char video_frame_buf[VIDEO_FRAME_SHM_SIZE];//视频帧数据
    unsigned int frame_size;//该帧的大小
}VideoFrame_t;

/**
 * @brief 共享内存的ID值枚举
 * 
 */
typedef enum __ShmId{
    VIDEO_FRAME = 0,//视频帧数据
    VIDEO_PARAM = 1,//视频参数
}ShmId_t;
// 函数声明
int InitCommonIPC(void);
#endif // !RPV_COM_H