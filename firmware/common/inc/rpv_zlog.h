#ifndef RPV_ZLOG_H
#define RPV_ZLOG_H
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "zlog.h"

// zlog配置文件
#define     ZLOG_CONF_FILEPATH      "/rpv/zlog/log.conf"
// zlog分类标识
#define     ZLOG_CATEGORY_PIV       "piv"       //piv进程
#define     ZLOG_CATEGORY_LOCODER   "locoder"   //locoder进程
#define     ZLOG_CATEGORY_PLF_CTRL  "platform_ctrl"   //locoder进程


// #define     ZLOG_PERROR(str)           

#endif // !RPV_ZLOG_H