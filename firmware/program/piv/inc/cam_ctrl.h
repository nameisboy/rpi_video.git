#ifndef CAM_CTRL_H
#define CAM_CTRL_H
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include "rpv_com.h"

// 摄像头设备节点
#define     CAM_VIDEO_DEV_PATH      "/dev/video0"
// 内存映射方式读取数据时，设置的映射帧数
#define     MMAP_FRAME_COUNT        3
/**
 * @brief 错误值枚举
 */
typedef enum __CameraErr_e
{
    CAM_SUC     =  0,
    CAM_ERR     = -1,

    // 摄像头状态相关错误枚举
    CAM_STATUS_ERR     = -10,//状态错误
    CAM_NOT_OPEN       = -11,//未打开
    // 设备相关错误没救
    CAM_DEV_NOT_FOUNT = -50,//找不到设备
    CAM_DEV_OPEN_ERR  = -51,//打开错误

    //设置参数失败
    CAM_SET_PARAM_FAIL = -80,//设置参数失败

    CAM_MMAP_FAIL = -100,//mmap映射失败
}CameraErr_t;

/**
 * @brief 摄像头状态
 */
typedef enum __CameraStatus_e
{
    CAM_STATUS_OPEN        = 1,//打开，获取到相应fd
    CAM_STATUS_READY       = 2,//就绪
    CAM_STATUS_CAPTURE     = 3,//视频捕获中
    CAM_STATUS_CLOSE       = 4,//关闭
}CameraStatus_t;

// 外部接口
CameraErr_t InitCamera(const CamParams_t cam_params);
CameraErr_t ReleaseCamera(void);
CameraErr_t ReadFrameFromCam(void *p_datas, unsigned int *p_data_len);
#endif

