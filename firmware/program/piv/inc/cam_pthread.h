#ifndef CAM_PTHREAD
#define CAM_PTHREAD
#include <pthread.h>
#include "cam_ctrl.h"
#include "rpv_com.h"

typedef enum{
    CAM_STATE_INIT      = 1,//初始化
    CAM_STATE_GET_DATAS = 2,//获取数据
    CAM_STATE_RELEASE   = 3,//释放
    CAM_STATE_IDLE      = 4,//空闲
}CamThreadStateEnum_t;

typedef int (*CamHandleCallBack)(int *, char *);
typedef struct __CamThreadStateHandler
{
    CamThreadStateEnum_t thread_state;//线程当前状态
    char *state_desc;//状态描述
    CamHandleCallBack cam_handler;//相关状态处理回调
}CamThreadStateHandler_t;

/**
 * @brief 处理状态
 * 
 */
typedef enum __HandleState{
    HANDLE_OK   = 0,
    HANDLE_FAIL = -1,
}HandleState_t;
// 函数声明
void *cam_thread(void *args);
#endif // !CAM_PTHREAD