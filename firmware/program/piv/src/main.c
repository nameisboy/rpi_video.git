#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include "cam_ctrl.h"
#include "rpv_com.h"
#include "cam_pthread.h"

static void InitZlog(void);
static void CreateThreads(void);
int main(int argc, char *argv[])
{
	char video_data_buf[VIDEO_FRAME_SHM_SIZE];
	unsigned int video_data_len = 0;
	CamParams_t cam_params;
	InitZlog();//初始化zlog
	InitCommonIPC();//初始化创建系统中IPC对象
	CreateThreads();
	while (1)
	{
		usleep(10*1000);
		// dzlog_notice("main thread sleeping..");
		// memset(video_data_buf, 0, sizeof(video_data_buf));
		// if(SHM_OK == ReadFromShm(VIDEO_PARAM_SHM_KEY, (void*)&cam_params, sizeof(cam_params))){
		// 	dzlog_debug("ReadFromShm suc, get the params of cam video:");
		// 	dzlog_info("cam_params.pix_fmt: \t[%#x]", cam_params.pix_fmt);
		// 	dzlog_info("cam_params.width: \t[%d]", cam_params.width);
		// 	dzlog_info("cam_params.height: \t[%d]", cam_params.height);
		// 	dzlog_info("cam_params.fps: \t\t[%d]", cam_params.fps);
		// }
	}
	exit(0);
}

static void CreateThreads(void)
{
	pthread_t cam_tid = -1;
	if(0 != pthread_create(&cam_tid, NULL, cam_thread, NULL)){
		dzlog_fatal("cam_thread create fail, thread_id = [%#x]", cam_tid);
	}
	dzlog_notice("cam_thread create suc, thread_id = [%#x]", cam_tid);
}

static void InitZlog(void)
{
	int ret = dzlog_init(ZLOG_CONF_FILEPATH, ZLOG_CATEGORY_PIV);
	if(0 != ret){
		dzlog_error("dzlog_init fail, ret: [%d]\n", ret);
	}
}