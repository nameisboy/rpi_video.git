#include "cam_ctrl.h"
#include "rpv_com.h"


typedef struct __FrameBufferInfo
{
    char *base_ptr;//起始地址
    unsigned int buffer_len;//长度
}FrameBufferInfo_t;

typedef struct 
{
    int cam_fd;//摄像头设备打开后的文件描述符
    CameraStatus_t cam_status;//摄像头设备状态
    CamParams_t cur_params;//当前参数
    FrameBufferInfo_t frame_buffs[MMAP_FRAME_COUNT];//帧缓冲
}CamObj_t;

static CamObj_t s_camera = {
    .cam_fd = -1,// 初始化-1
    .cam_status = CAM_STATUS_CLOSE,// 初始状态为关闭
};

static CameraErr_t OpenCamDev(const char *dev_path_ptr);
static CameraErr_t GetCamSupportParamList(void);
static CameraErr_t SetCameraParams(const CamParams_t set_params);
static CameraErr_t GetCameraParams(CamParams_t *get_params_p);
static CameraErr_t CameraMmapFrameBuffer(void);
static CameraErr_t CameraMunmapFrameBuffer(void);
static CameraErr_t CameraVideoStreamOn(void);
static CameraErr_t CameraVideoStreamOff(void);
CameraErr_t InitCamera(const CamParams_t cam_params)
{
    struct v4l2_capability tV4l2Cap;
    int t_err = -1;
    //1.打开摄像头
    if(CAM_SUC != OpenCamDev(CAM_VIDEO_DEV_PATH)){
        dzlog_error("OpenCamDev err, CAM_VIDEO_DEV_PATH: [%s]", CAM_VIDEO_DEV_PATH);
        return CAM_ERR;
    }
    //2.设置摄像头参数
    if(CAM_STATUS_OPEN != s_camera.cam_status){
        dzlog_error("camera is not opened!");
        return CAM_ERR;
    }
    //3.查询摄像头支持参数
    memset(&tV4l2Cap, 0, sizeof(struct v4l2_capability));
    t_err = ioctl(s_camera.cam_fd, VIDIOC_QUERYCAP, &tV4l2Cap);
    if (t_err) {
        dzlog_error("ioctl VIDIOC_QUERYCAP: %s", strerror(errno));
        return CAM_ERR;
    }
    if (!(tV4l2Cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)){// 是否支持视频采集
        dzlog_fatal("This device isn't support video capture!");
        return CAM_ERR;
    }
    //4.设置摄像头参数
    GetCamSupportParamList();
    CamParams_t get_params;
    GetCameraParams(&get_params);
    if(CAM_SUC != SetCameraParams(cam_params)){
        dzlog_error("Set camera params err, params:");
        dzlog_error("\t\t pix_fmt: \t[%#x]", cam_params.pix_fmt);
        dzlog_error("\t\t resolution: \t<%dx%d>", cam_params.width, cam_params.height);
        dzlog_error("\t\t fps: \t<%d>", cam_params.fps);
        return CAM_ERR;
    }
    s_camera.cur_params.pix_fmt = cam_params.pix_fmt;
    s_camera.cur_params.width   = cam_params.width;
    s_camera.cur_params.height  = cam_params.height;
    s_camera.cur_params.fps   = cam_params.fps;
    GetCameraParams(&get_params);
    //5.建立内存映射
    if(CAM_SUC != CameraMmapFrameBuffer()){
        dzlog_error("CameraMmapFrameBuffer err!");
        return CAM_ERR;
    }
    //6.开启视频流
    if(CAM_SUC != CameraVideoStreamOn()){
        dzlog_error("CameraVideoStreamOn err!");
        return CAM_ERR;
    }
    // 更新摄像头状态为ready
    s_camera.cam_status = CAM_STATUS_READY;

    return CAM_SUC;
}

/**
 * @brief 释放摄像头，完成相关清理工作
 * @return CameraErr_t 
 * @todo 优化为可多次调用
 */
CameraErr_t ReleaseCamera(void)
{
    if(CAM_STATUS_CLOSE == s_camera.cam_status){//摄像头已关闭, 表明已经执行过清理操作
        dzlog_fatal("Camera had close already, don't close multi times!");
        return CAM_ERR;
    }
    CameraVideoStreamOff();//关闭视频流
    CameraMunmapFrameBuffer();//取消内存映射
    if(s_camera.cam_fd >= 3){
        dzlog_notice("Now close cam_fd(%d)", s_camera.cam_fd);
        close(s_camera.cam_fd);
        s_camera.cam_fd = -1;//关闭后置-1
        s_camera.cam_status = CAM_STATUS_CLOSE;
        return CAM_SUC;
    }
    return CAM_ERR;
}

static CameraErr_t OpenCamDev(const char *dev_path_ptr)
{
    if(0 != access(dev_path_ptr, F_OK)){
        dzlog_error("access(): %s", strerror(errno));
        return CAM_DEV_NOT_FOUNT;
    }
    int fd = open(dev_path_ptr, O_RDWR);
    if(fd < 0){
        dzlog_error("open(): %s", strerror(errno));
        return CAM_DEV_OPEN_ERR;
    }
    // 更新状态
    s_camera.cam_fd = fd;
    s_camera.cam_status = CAM_STATUS_OPEN;

    return CAM_SUC;
}

/**
 * @brief 打印摄像头支持的参数列表，包括: 格式、分辨率、帧率
 * @return CameraErr_t
 */
static CameraErr_t GetCamSupportParamList(void)
{
    int fmt_ret = -1, frm_ret = -1, fps_ret = -1;
    struct v4l2_fmtdesc get_fmtdesc;
    struct v4l2_frmsizeenum get_frmsize;
    struct v4l2_frmivalenum get_frmival;
    if((s_camera.cam_status == CAM_STATUS_CLOSE) || s_camera.cam_fd < 3){
        dzlog_error("Camera status is err, can not read support formats!");
        return  CAM_STATUS_ERR;
    }
    memset(&get_fmtdesc, 0, sizeof(get_fmtdesc));
    get_fmtdesc.index = 0;
    get_fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    dzlog_info("=============== Support params info: ===============");
    while ((fmt_ret = ioctl(s_camera.cam_fd, VIDIOC_ENUM_FMT, &get_fmtdesc)) == 0) {//遍历出所有格式
        dzlog_info("\t format: \t[%#x]", get_fmtdesc.pixelformat);
        dzlog_info("\t desc: \t%s", get_fmtdesc.description);
        dzlog_info("\t support these resolution: ");
        memset(&get_frmsize, 0, sizeof(get_frmsize));
        get_frmsize.index = 0;
        get_frmsize.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        get_frmsize.pixel_format = get_fmtdesc.pixelformat;
        while((frm_ret = ioctl(s_camera.cam_fd, VIDIOC_ENUM_FRAMESIZES, &get_frmsize) == 0)){//遍历出当前格式下的所有分辨率
            dzlog_info("\t\t <%dx%d>", get_frmsize.discrete.width, get_frmsize.discrete.height);
            dzlog_info("\t\t support these fps: ");
            memset(&get_frmival, 0, sizeof(get_frmival));
            get_frmival.index = 0;
            get_frmival.width = get_frmsize.discrete.width;
            get_frmival.height = get_frmsize.discrete.height;
            while((fps_ret = ioctl(s_camera.cam_fd, VIDIOC_ENUM_FRAMEINTERVALS, &get_frmival)) == 0){//遍历出当前分辨率下的所有帧率
                dzlog_info("\t\t\t <%d>", get_frmival.discrete.denominator / get_frmival.discrete.numerator);
                get_frmival.index++;
            }
            get_frmsize.index++;
        }
        get_fmtdesc.index++;
    }
    dzlog_info("==============================================================");
    return  CAM_SUC;
}

/**
 * @brief 获取当前摄像头参数
 * @param  get_params_p     My Param doc
 * @return CameraErr_t 
 */
static CameraErr_t GetCameraParams(CamParams_t *get_params_p)
{
    struct v4l2_format fmt_get;
    struct v4l2_streamparm streamparm_get;
    unsigned int fps = 0;
    if(CAM_STATUS_CLOSE == s_camera.cam_status){
        dzlog_error("Camera is closed!");
        return CAM_NOT_OPEN;
    }
    memset(&fmt_get, 0, sizeof(struct v4l2_format));
    fmt_get.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;//需要先设置查询参数类型
    if(0 != ioctl(s_camera.cam_fd, VIDIOC_G_FMT, &fmt_get)){//查询格式与分辨率等参数
        dzlog_error("ioctl VIDIOC_G_FMT error: %s", strerror(errno));
        return CAM_ERR;
    }
    memset(&streamparm_get, 0, sizeof(struct v4l2_streamparm));
    streamparm_get.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(0 != ioctl(s_camera.cam_fd, VIDIOC_G_PARM, &streamparm_get)){//查询帧率等参数
        dzlog_error("ioctl VIDIOC_G_PARM error: %s", strerror(errno));
        return CAM_ERR;
    }
    fps = streamparm_get.parm.capture.timeperframe.denominator / streamparm_get.parm.capture.timeperframe.numerator;
    dzlog_notice("====================Get camera current params:====================");
    dzlog_notice("\t\t pixelformat: \t[%#x]", fmt_get.fmt.pix.pixelformat);
    dzlog_notice("\t\t resolution:\t <%dx%d>", fmt_get.fmt.pix.width, fmt_get.fmt.pix.height);
    dzlog_notice("\t\t fps:\t\t <%d>", fps);
    dzlog_notice("====================Get camera current params end=================");
    
    get_params_p->pix_fmt = fmt_get.fmt.pix.pixelformat;
    get_params_p->width   = fmt_get.fmt.pix.width;
    get_params_p->height  = fmt_get.fmt.pix.height;
    get_params_p->fps     = fps;

    return CAM_SUC;
}

/**
 * @brief 设置摄像头参数
 * @param  set_params       My Param doc
 * @return CameraErr_t 
 */
static CameraErr_t SetCameraParams(const CamParams_t set_params)
{
    struct v4l2_format fmt_set;
    struct v4l2_streamparm streamparm;

    if(CAM_STATUS_CLOSE == s_camera.cam_status){
        dzlog_error("Camera is closed!");
        return CAM_NOT_OPEN;
    }
    // 1.设置格式、分辨率
    memset(&fmt_set, 0, sizeof(struct v4l2_format));
    fmt_set.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;//需要先设置查询参数类型
    fmt_set.fmt.pix.pixelformat = set_params.pix_fmt;//格式
    fmt_set.fmt.pix.width       = set_params.width;//帧宽
    fmt_set.fmt.pix.height      = set_params.height;//帧高
    if(0 != ioctl(s_camera.cam_fd, VIDIOC_S_FMT, &fmt_set)){//查询格式与分辨率等参数
        dzlog_error("ioctl VIDIOC_S_FMT error: %s", strerror(errno));
        return CAM_SET_PARAM_FAIL;
    }
    // 2.设置帧率
    memset(&streamparm, 0, sizeof(struct v4l2_streamparm));
    streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ioctl(s_camera.cam_fd, VIDIOC_G_PARM, &streamparm);
    if (V4L2_CAP_TIMEPERFRAME & streamparm.parm.capture.capability) {/** 判断是否支持帧率设置 **/
        streamparm.parm.capture.timeperframe.numerator = 1;
        streamparm.parm.capture.timeperframe.denominator = set_params.fps;
        if (0 != ioctl(s_camera.cam_fd, VIDIOC_S_PARM, &streamparm)) {//设置帧率参数
            dzlog_error("ioctl VIDIOC_S_PARM error: %s", strerror(errno));
            return CAM_SET_PARAM_FAIL;
        }
    }else{
        dzlog_error("This camera is not support set fps!");
    }
    return CAM_SUC;
}

/**
 * @brief 为摄像头申请帧缓冲内存映射
 * @return CameraErr_t 
 */
static CameraErr_t CameraMmapFrameBuffer(void)
{
    struct v4l2_requestbuffers reqbuf;
    struct v4l2_buffer buf;

    // 1.申请帧缓冲
    reqbuf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuf.count  = MMAP_FRAME_COUNT;
    reqbuf.memory = V4L2_MEMORY_MMAP;
    if (0 > ioctl(s_camera.cam_fd, VIDIOC_REQBUFS, &reqbuf)) {
        dzlog_error("ioctl error: VIDIOC_REQBUFS: %s", strerror(errno));
        return CAM_MMAP_FAIL;
    }
    /* 2.建立内存映射 */
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    for (buf.index = 0; buf.index < MMAP_FRAME_COUNT; buf.index++) {
        ioctl(s_camera.cam_fd, VIDIOC_QUERYBUF, &buf);
        s_camera.frame_buffs[buf.index].buffer_len = buf.length;//获取该帧大小
        s_camera.frame_buffs[buf.index].base_ptr = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, s_camera.cam_fd, buf.m.offset);//该帧映射后的地址
        if (MAP_FAILED == s_camera.frame_buffs[buf.index].base_ptr) {
            dzlog_error("mmap error: %s", strerror(errno));
            return CAM_MMAP_FAIL;
        }
    }
    // 3.入队，填充到内核队列
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    for (buf.index = 0; buf.index < MMAP_FRAME_COUNT; buf.index++) {
        if (0 != ioctl(s_camera.cam_fd, VIDIOC_QBUF, &buf)) {
            dzlog_error("ioctl VIDIOC_QBUF error: %s", strerror(errno));
            return CAM_MMAP_FAIL;
        }
    }
    dzlog_notice("Camera mmap memory success!");
    return CAM_SUC;
}
/**
 * @brief 取消内存映射
 * @return CameraErr_t 
 */
static CameraErr_t CameraMunmapFrameBuffer(void)
{
    int i = 0;
    for(i=0; i<MMAP_FRAME_COUNT; i++){
        if(0 != munmap(s_camera.frame_buffs[i].base_ptr, s_camera.frame_buffs[i].buffer_len)){
            dzlog_error("munmap fail: %s", strerror(errno));
        }
    }
    return CAM_SUC;
}

/**
 * @brief 开启摄像头视频流
 * @return CameraErr_t 
 */
static CameraErr_t CameraVideoStreamOn(void)
{
    /* 开启流、摄像头开始采集数据 */
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (0 > ioctl(s_camera.cam_fd, VIDIOC_STREAMON, &type)) {
        dzlog_error("ioctl error: VIDIOC_STREAMON: %s", strerror(errno));
        return  CAM_ERR;
    }
    dzlog_notice("Camera stream on success!");
    return CAM_SUC;
}
/**
 * @brief 关闭摄像头视频流
 * @return CameraErr_t 
 */
static CameraErr_t CameraVideoStreamOff(void)
{
    /* 开启流、摄像头开始采集数据 */
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (0 > ioctl(s_camera.cam_fd, VIDIOC_STREAMOFF, &type)) {
        dzlog_error("ioctl error: VIDIOC_STREAMOFF: %s", strerror(errno));
        return  CAM_ERR;
    }
    dzlog_notice("Camera stream off success!");
    return CAM_SUC;
}

/**
 * @brief 读取摄像头视频帧
 * @param  p_datas      帧数据
 * @param  p_data_len   数据长度
 * @return CameraErr_t 
 */
CameraErr_t ReadFrameFromCam(void *p_datas, unsigned int *p_data_len)
{
    struct v4l2_buffer buf;
    if(s_camera.cam_fd < 3 || s_camera.cam_status != CAM_STATUS_READY){
        dzlog_fatal("Camera status is err, and cam_fd(%d) < 3", s_camera.cam_fd);
    }
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    if(0 != ioctl(s_camera.cam_fd, VIDIOC_DQBUF, &buf)){ //出队
        dzlog_error("ioctl error: VIDIOC_DQBUF: %s", strerror(errno));
        return  CAM_ERR;
    }
    dzlog_debug("buf index: [%d]", buf.index);
    dzlog_debug("buf len: [%d]", buf.length);
    memcpy(p_datas, s_camera.frame_buffs[buf.index].base_ptr, s_camera.frame_buffs[buf.index].buffer_len);
    *p_data_len = buf.length;
    // 数据处理完之后、将当前帧缓冲入队
    if(0 != ioctl(s_camera.cam_fd, VIDIOC_QBUF, &buf)){ //入队
        dzlog_error("ioctl error: VIDIOC_QBUF: %s", strerror(errno));
        return  CAM_ERR;
    }

    return CAM_SUC;
}

