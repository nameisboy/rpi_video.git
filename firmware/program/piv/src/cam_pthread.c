#include "cam_pthread.h"

static CamParams_t s_cam_param = {
    .pix_fmt = V4L2_PIX_FMT_YUYV,//帧格式：YUYV
    .width   = 640,//宽
    .height  = 480,//高
    .fps     = 20,//帧率
};

// 函数声明
static int HandleInit(int *next_state_p, char *state_desc_p);
static int HandleGetDatas(int *next_state_p, char *state_desc_p);
static int HandleRelease(int *next_state_p, char *state_desc_p);
static int HandleIDLE(int *next_state_p, char *state_desc_p);

// 摄像头状态处理列表
CamThreadStateHandler_t s_cam_thread_handler_list[] = {
    {CAM_STATE_INIT,      "CAM_STATE_INIT[1]",      HandleInit    },
    {CAM_STATE_GET_DATAS, "CAM_STATE_GET_DATAS[2]", HandleGetDatas},
    {CAM_STATE_RELEASE,   "CAM_STATE_RELEASE[3]",   HandleRelease },
    {CAM_STATE_IDLE,      "CAM_STATE_IDLE[4]",      HandleRelease },
};

void *cam_thread(void *args)
{
    int i = 0;
    int handler_ret = -1;
    int handle_err_cnt = 0;
    CamThreadStateEnum_t thread_state = CAM_STATE_INIT;
    int array_size = sizeof(s_cam_thread_handler_list)/sizeof(s_cam_thread_handler_list[0]);
    while (1)
    {
        for(i=0; i<array_size; i++){
            //匹配状态，根据状态进行相应操作
            if(thread_state == s_cam_thread_handler_list[i].thread_state){
                handler_ret = s_cam_thread_handler_list[i].cam_handler((int*)&thread_state, s_cam_thread_handler_list[i].state_desc);
                if(HANDLE_FAIL == handler_ret){// 处理出错
                    usleep(10*1000);//延时10ms
                    handle_err_cnt++;
                    break;
                }
            }
        }
        usleep(5*1000);
        if(handle_err_cnt >= 3){
            dzlog_notice("handle fail more than  %d times, will be IDLE!\n", handle_err_cnt);
            handle_err_cnt = 0;
            thread_state = CAM_STATE_IDLE;
        }
    }
    pthread_exit(NULL);
}
/**
 * @brief 初始化状态处理函数
 * 
 * @param next_state_p 
 * @param state_desc_p 
 * @return int 
 */
static int HandleInit(int *next_state_p, char *state_desc_p)
{
    dzlog_notice("current state: %s\n", state_desc_p);
    if(CAM_SUC != InitCamera(s_cam_param)){
        dzlog_notice("Init camera fail!\n");
        return HANDLE_FAIL;
    }
    dzlog_notice("Init camera successful!\n");
    SemBinaryP(VIDEO_PARAM_SEM_KEY, BLOCK_WAIT);//上锁
    if(SHM_OK != WriteToShm(VIDEO_PARAM_SHM_KEY, &s_cam_param, sizeof(s_cam_param))){//将视频参数更新到共享内存
        dzlog_error("WriteToShm fail, shm key: [%#x]", VIDEO_PARAM_SHM_KEY);
        SemBinaryV(VIDEO_PARAM_SEM_KEY);//解锁
        return HANDLE_FAIL;
    }
    SemBinaryV(VIDEO_PARAM_SEM_KEY);//解锁
    *next_state_p = CAM_STATE_GET_DATAS;
    return HANDLE_OK;
}
/**
 * @brief 获取数据状态处理函数
 * 
 * @param next_state_p 
 * @param state_desc_p 
 * @return int 
 */
static int HandleGetDatas(int *next_state_p, char *state_desc_p)
{
    VideoFrame_t video_frame;
    unsigned int data_len = 0;
    // dzlog_notice("current state: %s\n", state_desc_p);
    memset(&video_frame, 0, sizeof(video_frame));
    if(CAM_SUC != ReadFrameFromCam(video_frame.video_frame_buf, &data_len)){
        dzlog_notice("ReadFrameFromCam fail!\n");
        return HANDLE_FAIL;
    }
    video_frame.frame_size = data_len;//帧长度
    SemBinaryP(VIDEO_FRAME_SEM_KEY, BLOCK_WAIT);//上锁
    if(SHM_OK != WriteToShm(VIDEO_FRAME_SHM_KEY, &video_frame, sizeof(video_frame))){
        dzlog_notice("WriteToShm err, shm key = [%#x]\n", VIDEO_FRAME_SHM_KEY);
        SemBinaryV(VIDEO_FRAME_SEM_KEY);
        return HANDLE_FAIL;
    }
    SemBinaryV(VIDEO_FRAME_SEM_KEY);
    return HANDLE_OK;
}
/**
 * @brief 释放操作处理函数
 * 
 * @param next_state_p 
 * @param state_desc_p 
 * @return int 
 */
static int HandleRelease(int *next_state_p, char *state_desc_p)
{
    dzlog_notice("current state: %s\n", state_desc_p);
    if(CAM_SUC != ReleaseCamera()){
        dzlog_notice("Release camera fail!\n");
        return HANDLE_FAIL;
    }
    *next_state_p = CAM_STATE_IDLE;
    dzlog_notice("Release camera successful!\n");
    return HANDLE_OK;
}
/**
 * @brief 空闲状态处理
 * 
 * @param next_state_p 
 * @param state_desc_p 
 * @return int 
 */
static int HandleIDLE(int *next_state_p, char *state_desc_p)
{
    static int sleep_cnt = 0;
    sleep_cnt++;
    if(sleep_cnt >= 1000){
        dzlog_notice("current state: %s\n", state_desc_p);
        sleep_cnt = 0;
    }
    return HANDLE_OK;
}