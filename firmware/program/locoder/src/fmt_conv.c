#include "fmt_conv.h"
#include <string.h>


ConvResult_t YUYVConv2YUV420p(int width,int hight,char *yuy2,char *yuv420)
{
    memset(yuv420,0,sizeof(char)*width*hight*3/2);
    char *u = yuv420+hight*width;
    char *v = u+hight*width/4;
    int i=0,j=0;

    for (i = 0; i <hight/2; i++) {
        char *src_l1=(yuy2+width*2*2*i);
        char *src_l2=(src_l1+width*2);
        char *y_l1=yuv420+width*2*i;
        char *y_l2=y_l1+width;
        for(j=0;j<width/2;j++)
        {
            *y_l1++=src_l1[0];
            *u++=src_l1[1];
            *y_l1++=src_l1[2];
            *v++=src_l1[3];
            *y_l2++=src_l2[0];
            *y_l2++=src_l2[2];
            src_l1+=4;
            src_l2+=4;
        }
    }
    return CONV_OK;
}
ConvResult_t YUV420pConv2YUV420sp(unsigned char* yuv420p, unsigned char* yuv420sp, int width, int height)
{
    int i, j;
    int y_size = width * height;

    unsigned char* y = yuv420p;
    unsigned char* u = yuv420p + y_size;
    unsigned char* v = yuv420p + y_size * 5 / 4;
    unsigned char* y_tmp = yuv420sp;
    unsigned char* uv_tmp = yuv420sp + y_size;

    memcpy(y_tmp, y, y_size);// y分量
    for (j = 0, i = 0; j < y_size/2; j+=2, i++)// u、v分量
    {
	// 此处可调整U、V的位置，变成NV12或NV21
#if 01
        uv_tmp[j] = u[i];
        uv_tmp[j+1] = v[i];
#else
        uv_tmp[j] = v[i];
        uv_tmp[j+1] = u[i];
#endif
    }
    return CONV_OK;
}

ConvResult_t YUV420spConv2YUV420p(unsigned char* yuv420sp, unsigned char* yuv420p, int width, int height)
{
    int i, j;
    int y_size = width * height;

    unsigned char* y = yuv420sp;
    unsigned char* uv = yuv420sp + y_size;
    unsigned char* y_tmp = yuv420p;
    unsigned char* u_tmp = yuv420p + y_size;
    unsigned char* v_tmp = yuv420p + y_size * 5 / 4;

    // y
    memcpy(y_tmp, y, y_size);
    // u
    for (j = 0, i = 0; j < y_size/2; j+=2, i++)
    {
        u_tmp[i] = uv[j];
        v_tmp[i] = uv[j+1];
    }
    return CONV_OK;
}
