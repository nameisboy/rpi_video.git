#include "srsRtmp.h"

static char push_url[1024] = {0};
static srs_rtmp_t __srs_rtmp_obj = NULL;
static int dts = 0;//dts、pts用于控制拉流端的播放速度
static int pts = 0;
static int sendH264_state = 0;//H264推送状态

// sps、pps帧信息
#define     SPS_PPS_FRAME_SIZE      1024
static char sps_pps_framebuf[SPS_PPS_FRAME_SIZE] = {0};
static RtmpH264Stream_t sps_pps_h264_frame = {
    .pH264_data = NULL,
    .data_size  = 0,
};//保存H264的sps、pps，重连时重新发送该帧

// 内部函数声明
static int IsFrameIncludeSPSorPPS(RtmpH264Stream_t *pH264);
/**
 * @brief 使用srs_rtmp连接服务器
 * 
 * @param pushUrl 连接后需要推送的url
 * @return int 
 *      返回0成功，否则返回小于0的错误码，见 SrsRtmpErr_t
 */
int SrsRtmpConnect(const char *pushUrl)
{
    if(pushUrl == NULL){
        dzlog_fatal("pushUrl is null point: [%p]", pushUrl);
        return ERR_SRS_URL_ERR;
    }
    // 1.创建Hanlder
    if(NULL == (__srs_rtmp_obj = srs_rtmp_create(RPI_PUSH_URL))){
        dzlog_fatal("srs_rtmp_create fail, ret __srs_rtmp_obj: [%p]", __srs_rtmp_obj);
        return ERR_SRS_CREAT;
    }
    // 2.握手
    if(0 != srs_rtmp_handshake(__srs_rtmp_obj)){
        dzlog_fatal("srs_rtmp_handshake fail");
        srs_human_trace("simple handshake failed.");
        return ERR_SRS_HANDSHAKE_FAIL;
    }
    dzlog_info("simple handshake success");
    // 3.连接
    if(0 != srs_rtmp_connect_app(__srs_rtmp_obj)){
        dzlog_fatal("srs_rtmp_connect_app fail");
        return ERR_SRS_CONNC_APP_FAIL;
    }
    dzlog_info("RTMP connect success");
    // 4.发布流
    if(0 != srs_rtmp_publish_stream(__srs_rtmp_obj)){
        dzlog_fatal("srs_rtmp_publish_stream fail");
        return ERR_SRS_PUB_STREAM_FAIL;
    }
    dzlog_info("publish stream success");

    // char url_buf[1024] = {0};
    // memset(url_buf, 0, sizeof(url_buf));
    // memset(push_url, 0, sizeof(push_url));
    // printf("pushUrl = [%s]\r\n", pushUrl);
    // strncpy(url_buf, pushUrl, strlen(pushUrl));
    // dzlog_info("url_buf: [%s].", url_buf);
    // strncpy(push_url, url_buf, strlen(url_buf));
    // dzlog_info("Get push_url: %s", push_url);

    return SRS_STATE_SUC;
}

#define     SRS_RTMP_FPS    (20)
/**
 * @brief 推送H264视频帧到服务器
 * 
 * @param pH264 需要推送的H264视频流
 * @return int 
 *      返回0成功，否则返回小于0的错误码，见 SrsRtmpErr_t
 */
int SrsRtmpSendH264(RtmpH264Stream_t *pH264)
{

    dts += 1000 / SRS_RTMP_FPS;
    pts  = dts;
    if(NULL == pH264 || NULL == pH264->pH264_data){
        dzlog_error("Point null error, pH264: [%p], pH264->pH264_data: [%p].", pH264, pH264->pH264_data);
        return SRS_STATE_ERR;
    }
    // if(0 == IsFrameIncludeSPSorPPS(pH264)){//检查此帧是否包含sps、pps,包含则保存起来，出现3043错误时，在重连函数发送该帧
    //     memset(sps_pps_framebuf, 0, sizeof(sps_pps_framebuf));
    //     memcpy(sps_pps_framebuf, pH264->pH264_data, pH264->data_size);
    //     sps_pps_h264_frame.pH264_data = sps_pps_framebuf;
    //     sps_pps_h264_frame.data_size  = pH264->data_size;
    //     dzlog_info("Get sps or pps frame, size: [%d].", sps_pps_h264_frame.data_size);
    // }
    int ret = srs_h264_write_raw_frames(__srs_rtmp_obj, pH264->pH264_data, pH264->data_size, dts, pts);
    sendH264_state = ret;
    if(0 != ret){
        if (srs_h264_is_dvbsp_error(ret)) {
            // srs_human_trace("ignore drop video error, code=%d", ret);
            dzlog_warn("ignore drop video error, code=%d", ret);
        } else if (srs_h264_is_duplicated_sps_error(ret)) {
            // srs_human_trace("ignore duplicated sps, code=%d", ret);
            dzlog_warn("ignore duplicated sps, code=%d", ret);
        } else if (srs_h264_is_duplicated_pps_error(ret)) {
            // srs_human_trace("ignore duplicated pps, code=%d", ret);
            dzlog_warn("ignore duplicated pps, code=%d", ret);
        } else {
            // srs_human_trace("send h264 raw data failed. ret=%d", ret);
            dzlog_warn("send h264 raw data failed. ret=%d", ret);
            dzlog_error("dts = [%d], pts = [%d]", dts, pts);
            dzlog_info("Will return ERR_SRS_SEND_H264_FAIL.");
            return ERR_SRS_SEND_H264_FAIL;
        }
    }
#ifdef DBG_RTMP_ERR    
    static int cnt = 0, mark_var = 0;
    if(++cnt >= 100 && mark_var == 0){
        sendH264_state = ERROR_SOCKET_WRITE;
        cnt = 0;
        mark_var = 1;
    }
#endif
    return SRS_STATE_SUC;
}

/**
 * @brief srs_rtmp关闭连接
 * 
 * @param args
 * @return int 
 *      返回0成功，否则返回小于0的错误码，见 SrsRtmpErr_t
 */
int SrsRtmpClose(void *args)
{
    if(NULL == __srs_rtmp_obj){
        dzlog_fatal("__srs_rtmp_obj[%p] is NULL.", __srs_rtmp_obj);
        return SRS_STATE_ERR;
    }
    srs_rtmp_destroy(__srs_rtmp_obj);
    __srs_rtmp_obj = NULL;
    dzlog_info("SrsRtmpClose success.");

    return SRS_STATE_SUC;
}

/**
 * @brief srs_rtmp重连、错误处理
 * 
 * @param sendState 上一次的发送状态
 * @return int 
 *      返回0成功，否则返回小于0的错误码，见 SrsRtmpErr_t
 */
int SrsRtmpReconnect(int sendState)
{
    //无错误，不需要处理
    if(sendH264_state == 0){
        dzlog_debug("sendH264_state[%d] is zero, connection is valid.", sendH264_state);
        return 0;
    }
    switch (sendH264_state){
        case ERROR_SOCKET_WRITE:{//写socket错误
            system("echo SrsRtmpReconnect: `date` >> /rpv/log/reconnect.log&");
            usleep(1000*1000);
            dzlog_warn("State: ERROR_SOCKET_WRITE[%d], will close and connect server again!", ERROR_SOCKET_WRITE);
            SrsRtmpClose(NULL);
            dts = 0; pts = 0;
            if(0 != SrsRtmpConnect(RPI_PUSH_URL)){
                dzlog_fatal("SrsRtmpConnect fail, return error.");
                return ERR_SRS_RECONNECT_FAIL;
            }
            dzlog_info("Connect server success again!");
            return SRS_STATE_SUC;
        }break;
        case ERROR_H264_DROP_BEFORE_SPS_PPS:{//错误的H264帧(未发送sps、pps推送H264后续帧导致)
            //尝试发送pps、sps帧
            // if(sps_pps_h264_frame.pH264_data == NULL){
            //     dzlog_fatal("sps_pps_h264_frame.pH264_data[%p] is nil, return error.", sps_pps_h264_frame.pH264_data);
            //     return ERR_SRS_RECONNECT_FAIL;
            // }
            // int i = 0;
            // for(i=0; i<3; i++){
            //     if(0 != SrsRtmpSendH264(&sps_pps_h264_frame))
            //         continue;
            //     else 
            //         break;
            //     dzlog_fatal("Send sps、pps frame fail, return error.");
            //     return ERR_SRS_RECONNECT_FAIL;
            // }
        }break;
        case ERROR_H264_DUPLICATED_PPS:{

        }break;
        default:{
            dzlog_warn("An unrecognized error, code: [%d].", sendH264_state);
        }break;
    }

    dzlog_debug("Don't Reconnect!");
    return SRS_STATE_SUC;
}

// 追加写入文件
static void AppendWriteToFile(const char *p_filename, char *p_buf, unsigned int size)
{
    int fd = -1;
    if(0 > (fd = open(p_filename, O_CREAT | O_WRONLY | O_APPEND, 0777))){
        perror("open()");
        return;
    }
    if(size != write(fd, p_buf, size)){
        perror("write()");
        close(fd);
        return;
    }
    close(fd);
}
#if 0
// 此函数某些情况下会coredump，待修复
static int IsFrameIncludeSPSorPPS(RtmpH264Stream_t *pH264)
{
    // dzlog_warn("debug111");
    if(NULL == pH264 || NULL == pH264->pH264_data){
        dzlog_fatal("Some points maybe nil, pH264: [%p], pH264->pH264_data: [%p]", pH264, pH264->pH264_data);
        return -1;
    }
    char *pNalStart = NULL;
    char nal_start = 0;
    // dzlog_warn("debug111");

    // 1.检查起始码，首先检查0x00 00 00 01格式
    if (pH264->pH264_data[0] == 0x00 && pH264->pH264_data[1] == 0x00 && 
        pH264->pH264_data[2] == 0x00 && pH264->pH264_data[3] == 0x01) {
        // dzlog_warn("debug111");
		// pNalStart = pH264->pH264_data+4;
        nal_start = pH264->pH264_data[4];
	}
    // dzlog_warn("debug111");
    // 检查0x00 00 01格式
    if (pH264->pH264_data[0] == 0x00 && pH264->pH264_data[1] == 0x00 && pH264->pH264_data[2] == 0x01) {
        // dzlog_warn("debug111");
        nal_start = pH264->pH264_data[3];
		// pNalStart = pH264->pH264_data+3;
	}
    // dzlog_warn("debug 222");
    // 2.检查是否为sps、pps
    char nal_type = 0;
    // dzlog_warn("======> pNalStart = [%p]", pNalStart);
    // dzlog_warn("======> pH264->pH264_data = [%p].", pH264->pH264_data);
    // dzlog_info("======> pH264->data_size = [%d].", pH264->data_size);
    // nal_type = pNalStart[0] & 0x1f;
    nal_type = nal_start & 0x1f;
    // dzlog_warn("debug 222");
    switch (nal_type){
        case 7:
            dzlog_info("The frame include SPS info.");
            break;
        case 8:
            dzlog_info("The frame include PPS info.");
            break;
        default:
            dzlog_debug("The frame include no SPS or PPS info.");
            return -1;
            break;
    }
    dzlog_notice("AppendWriteToFile sps or pps, size: [%d].", pH264->data_size);
    AppendWriteToFile("/rpv/log/video/sps.h264", pH264->pH264_data, pH264->data_size);
    return 0;
}
#endif