#include <pthread.h>
#include "rpv_com.h"
#include "upload_thread.h"

static void InitZlog(void);
static void CreateThreads(void);
int main(int argc, char *argv[])
{
    InitZlog();
    InitCommonIPC();
    CreateThreads();
    while (1){
        usleep(10*1000);  
        dzlog_debug("locoder main thread running...");
    }    
    exit(0);
}

static void CreateThreads(void)
{
    pthread_t upload_tid = -1;
    if(0 != pthread_create(&upload_tid, NULL, upload_thread, NULL)){
        dzlog_fatal("upload_thread create failed!");
        exit(-1);
    }
    dzlog_notice("upload_thread thread id: [%#x]", upload_tid);
}

static void InitZlog(void)
{
	int ret = dzlog_init(ZLOG_CONF_FILEPATH, ZLOG_CATEGORY_LOCODER);
	if(0 != ret){
		dzlog_error("dzlog_init fail, ret: [%d]\n", ret);
	}
}