#include "codec.h"

// sps、pps信息
static char sps_pps_info[] = {
0x00, 0x00, 0x00, 0x01, 0x27, 0x64, 0x00, 0x28, 0xac, 0x2b, 0x40, 0x50, 0x1e, 0xd0, 0x0f, 0x12,
0x26, 0xa0, 0x00, 0x00, 0x00, 0x01, 0x28, 0xee, 0x02, 0x5c, 0xb0,
};

static Codec_t s_codec = {
    .pEncCtx = NULL,
    .pAvPkt  = NULL,
    .pCodec  = NULL,
    .pAvFrame = NULL,
};

// 初始化编码器
int InitFFmpegEncoder(Codec_t *pEncoder)
{
    int i, ret, x, y;
    AVCodec *codec;
    AVCodecContext *c = NULL;
    AVPacket *pkt  = NULL;
    AVFrame *frame = NULL;
    // 1.查找编码器
    codec = avcodec_find_encoder_by_name(CODEC_NAME);
    if (!codec) {
        dzlog_fatal("avcodec_find_encoder_by_name fail, codec = [%p]", codec);
        return -1;
    }
    // 2.为编码器分配上下文
    c = avcodec_alloc_context3(codec);
    if (!c) {
        dzlog_fatal("Could not allocate video codec context\n");
        return -1;
    }
    // avcodec_open2前设置sps、pps添加到每个IDR帧之前.
    // c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;//在extradata中放置全局头文件，而不是每个关键帧。
    c->extradata_size = sizeof(sps_pps_info);
    dzlog_info("c->extradata_size: [%d].", c->extradata_size);
    c->extradata = (uint8_t*)av_malloc(sizeof(sps_pps_info)+ AV_INPUT_BUFFER_PADDING_SIZE);
    if (c->extradata == NULL) {
    	dzlog_fatal("could not av_malloc the video params extradata!\n");
    	return -1;
    }
    memcpy(c->extradata, sps_pps_info, sizeof(sps_pps_info));
    // 3.设置编码器参数
    /* put sample parameters */
    c->bit_rate = 400000;
    /* resolution must be a multiple of two */
    c->width = YUV_WIDTH;
    c->height = YUV_HEIGHT;
    /* frames per second */
    c->time_base = (AVRational){1, 25};
    c->framerate = (AVRational){25, 1};
    c->gop_size = 10;
    c->max_b_frames = 1;
    c->pix_fmt = AV_PIX_FMT_YUV420P;
    //如果是h264 设置编码的预设参数 慢编码
    if (codec->id == AV_CODEC_ID_H264)
        av_opt_set(c->priv_data, "preset", "slow", 0);
    // 4.打开编码器
    ret = avcodec_open2(c, codec, NULL);
    if (ret < 0) {
        dzlog_fatal("Could not open codec: %s\n", av_err2str(ret));
        return -1;
    }
    // 5.分配AVPacket、AVFrame
    pkt = av_packet_alloc();
    if (!pkt) {
        dzlog_fatal("Could not allocate video frame\n");
        exit(1);
    }
    frame = av_frame_alloc();
    if (!frame) {
        dzlog_fatal("Could not allocate video frame\n");
        exit(1);
    }
    // 6.为frame分配buffer
    frame->format = c->pix_fmt;
    frame->width  = c->width;
    frame->height = c->height;
    ret = av_frame_get_buffer(frame, 0);
    if (ret < 0) {
        dzlog_fatal("Could not allocate the video frame data\n");
        exit(1);
    }
    // 7.计算每一帧数据的大小
    int frame_bytes = av_image_get_buffer_size(frame->format, frame->width,
                                            frame->height, 1);
    dzlog_info("frame_bytes %d\n", frame_bytes);
    pEncoder->pAvFrame = frame;
    pEncoder->pAvPkt   = pkt;
    pEncoder->pCodec   = codec;
    pEncoder->pEncCtx  = c;

    return 0;
}
#define     H264_FILEPATH   "/rpv/log/video/test.h264"
static void AppendWriteToFile(const char *p_filename, char *p_buf, unsigned int size);
// 编码函数
int Yuv420pEncodeToH264(Codec_t *pCodec, char *pYuvbuf, unsigned char *pH264buf, int *pH264size)
{
    dzlog_debug("pAvFrame: %p\n", pCodec->pAvFrame);
    int ret;
    // 1.检查帧是否有效、可写
    if(0 == av_frame_is_writable(pCodec->pAvFrame)){//该帧不可写
        if(0 != av_frame_make_writable(pCodec->pAvFrame)){
            dzlog_fatal("av_frame_make_writable failed\n");
            return -1;
        }
    }
    if (pCodec->pAvFrame)
        dzlog_debug("Send frame %3"PRId64"\n", pCodec->pAvFrame->pts);
    // 2.存储⼀帧像素数据存储到AVFrame对应的data buffer
    int need_size = av_image_fill_arrays(pCodec->pAvFrame->data, pCodec->pAvFrame->linesize, 
                                            pYuvbuf, pCodec->pAvFrame->format,
                                            pCodec->pAvFrame->width, pCodec->pAvFrame->height, 1);
    pCodec->pAvFrame->pts += 40;
    // 3.将AVFrame⾮压缩数据给编码器
    ret = avcodec_send_frame(pCodec->pEncCtx, pCodec->pAvFrame);
    if (ret < 0) {
        dzlog_error("Error sending a frame for encoding\n");
        exit(1);
    }
    // 4.读取编码后的数据
    while (ret >= 0) {
        ret = avcodec_receive_packet(pCodec->pEncCtx, pCodec->pAvPkt);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return;
        else if (ret < 0) {
            dzlog_error("Error during encoding\n");
            exit(1);
        }
        if((pCodec->pAvPkt->flags & AV_PKT_FLAG_KEY) == 1){//判断关键帧，手动在帧前添加SPS、PPS信息
            uint8_t *tmp_data_p = av_malloc(pCodec->pEncCtx->extradata_size+pCodec->pAvPkt->size);
            if (!tmp_data_p) {
                dzlog_fatal("Could not allocate video frame\n");
                return (-1);
            }
            memcpy(tmp_data_p, pCodec->pEncCtx->extradata, pCodec->pEncCtx->extradata_size);
            memcpy(tmp_data_p+pCodec->pEncCtx->extradata_size, pCodec->pAvPkt->data, pCodec->pAvPkt->size);
            int size =  pCodec->pAvPkt->size + pCodec->pEncCtx->extradata_size;
            memcpy(pH264buf, tmp_data_p, size);
            *pH264size = size;
            av_packet_unref(pCodec->pAvPkt);
            av_freep(&tmp_data_p);//!!!av_freep传入指针地址，av_free传入地址，否则会引起coredump
            return 0;
        }
        memcpy(pH264buf, pCodec->pAvPkt->data, pCodec->pAvPkt->size);
        *pH264size = pCodec->pAvPkt->size;
        // AppendWriteToFile(H264_FILEPATH, pH264buf, pCodec->pAvPkt->size);
        dzlog_debug("Write packet %3"PRId64" dts: %3"PRId64" (size=%5d)\n", pCodec->pAvPkt->pts, pCodec->pAvPkt->dts, pCodec->pAvPkt->size);
        av_packet_unref(pCodec->pAvPkt);
        return 0;
    }
}
// 释放编码器
int ReleaseFFmepgEncoder(Codec_t *pCodec)
{ 
    if(NULL == pCodec){
        dzlog_error("pCodec null");
        return -1;
    }
    if(NULL != pCodec->pEncCtx){
        avcodec_close(pCodec->pEncCtx);//关闭编码器
    }
    if(NULL != pCodec->pCodec){
        avcodec_free_context(&pCodec->pEncCtx);
    }
    if(NULL != pCodec->pAvFrame){
        av_frame_free(&pCodec->pAvFrame);
    }
    if(NULL != pCodec->pAvPkt){
        av_packet_free(&pCodec->pAvPkt);
    }
    return 0;
}

// 追加写入文件
static void AppendWriteToFile(const char *p_filename, char *p_buf, unsigned int size)
{
    int fd = -1;
    if(0 > (fd = open(p_filename, O_CREAT | O_WRONLY | O_APPEND, 0777))){
        perror("open()");
        return;
    }
    if(size != write(fd, p_buf, size)){
        perror("write()");
        close(fd);
        return;
    }
    close(fd);
}
