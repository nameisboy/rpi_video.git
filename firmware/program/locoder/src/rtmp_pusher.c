#include "rtmp_pusher.h"

/**
 * @brief rtmp推流器初始化函数
 * 
 * @param pRtmp 
 * @return int 
 *      返回0成功，否则失败(Todo: 返回值枚举定义)
 */
int RtmpPusherInit(RtmpPusherCb_t *pRtmp)
{
    if(NULL == pRtmp){
        dzlog_fatal("pRtmp is nil: [%p].", pRtmp);
        return -1;
    }
    return pRtmp->HowToConnectServer(pRtmp->push_url);
}
/**
 * @brief Rtmp运行函数，循环运行发送H264
 * 
 * @param pRtmp 
 * @param pH264 
 * @return int 
 *      返回0成功，否则失败(Todo: 返回值枚举定义)
 */
int RtmpPusherRun(RtmpPusherCb_t *pRtmp, RtmpH264Stream_t *pH264)
{
    if(NULL == pH264){
        dzlog_fatal("H264 data point nil, pH264: [%p].", pH264);
        return -1;
    }
    int ret = pRtmp->HowToPushH264(pH264);
    if(0 != pRtmp->HowToReconnect(ret)){//检查推送状态，发送错误可重连
        dzlog_fatal("Rtmp pusher reconnect fail.");
        return -1;
    }
    return 0;
}