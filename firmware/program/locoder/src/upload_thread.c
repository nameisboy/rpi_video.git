#include <pthread.h>
#include "upload_thread.h"
#include "fmt_conv.h"
#include "codec.h"
#include "srsRtmp.h"

static int InitVideoMemory(VideoMemory_t *pVm);
static int DestroyVideoMemory(VideoMemory_t *pVm);
static int ProcessInit(LocoderProcess_t *pLocoder);
static int ProcessExit(LocoderProcess_t *pLocoder);
static void AppendWriteToFile(const char *p_filename, char *p_buf, unsigned int size);

void *upload_thread(void *args)
{
    char H264_buf[1024*1024] = {0};
    uint32_t h264_buf_size = 0;
    RtmpH264Stream_t rtmp_h264;
    LocoderProcess_t locoder;
    VideoFrame_t video_frame;

    if(0 != ProcessInit(&locoder)){
        dzlog_fatal("locoder ProcessInit fail, now exit.");
        exit(-1);
    }
    while (1){
        dzlog_debug("locoder upload_thread running...");
        memset(&video_frame, 0, sizeof(video_frame));
        if(SHM_OK == ReadFromShm(VIDEO_FRAME_SHM_KEY, &video_frame, sizeof(video_frame))){
            YUYVConv2YUV420p(locoder.cam_param.width, locoder.cam_param.height, video_frame.video_frame_buf, locoder.vm.pYuv420);
            // AppendWriteToFile(YUV420_FILE_PATH, p_yuv420, cam_params.width*cam_params.height*3/2);
            memset(H264_buf, 0, sizeof(H264_buf));
            Yuv420pEncodeToH264(&locoder.coder, locoder.vm.pYuv420, H264_buf, &h264_buf_size);
            dzlog_info("h264_buf_size = [%d]", h264_buf_size);
            if(0 >= h264_buf_size){//编码出来的大小小于等于0
                dzlog_fatal("h264_buf_size(%d) is abnormal.", h264_buf_size);
                usleep(100*1000);
                continue;
            }
            memset(&rtmp_h264, 0, sizeof(RtmpH264Stream_t));
            rtmp_h264.pH264_data = H264_buf;
            rtmp_h264.data_size = h264_buf_size;
            if(0 != RtmpPusherRun(&locoder.rtmp_cb, &rtmp_h264)){
                dzlog_fatal("RtmpPusherRun fail, program exit.");
                exit(EXIT_FAILURE);
            }

            usleep(40*1000);
        }
    }
    dzlog_info("Now ProcessExit.");
    ProcessExit(&locoder);
    pthread_exit(NULL);
}

/**
 * @brief 初始化视频内存，使用calloc申请需要的内存大小，需要填充pVm中各个size
 * 
 * @param pVm 
 * @return int 返回0成功，否则失败
 */
static int InitVideoMemory(VideoMemory_t *pVm)
{
    int ret = 0;
    if(NULL == pVm || 0 == pVm->yuv420_size){//检查参数合法性
        return -1;
    }
    if(NULL == (pVm->pYuv420 = calloc(1, pVm->yuv420_size))){//检查申请内存的结果
        dzlog_fatal("calloc fail, err: %s", strerror(errno));
        ret |= 1;
    }
    dzlog_notice("calloc pVm->pYuv420 successful, addr: [%p]", pVm->pYuv420);
    
    if(0 != ret){//有内存申请失败，释放申请成功的内存
        DestroyVideoMemory(pVm);
        return -1;
    }
    return 0;
}
/**
 * @brief 释放、销毁视频相关内存
 * 
 * @param pVm 
 * @return int 返回0成功，否则失败
 */
static int DestroyVideoMemory(VideoMemory_t *pVm)
{
    if(NULL == pVm){
        dzlog_fatal("pVm is NULL, return destroy fail!");
        return -1;
    }
    if(NULL != pVm->pYuv420){
        free(pVm->pYuv420);
        pVm->pYuv420 = NULL;
    }
    return 0;
}
static int ProcessInit(LocoderProcess_t *pLocoder)
{
    memset(pLocoder, 0, sizeof(LocoderProcess_t));
    if(SHM_OK != ReadFromShm(VIDEO_PARAM_SHM_KEY, &pLocoder->cam_param, sizeof(CamParams_t))){
        dzlog_error("ReadFromShm fail!");
        return -1;
    }
    dzlog_notice("===============cam params===============");
    dzlog_notice("cam_params.pix_fmt = [%#x]",pLocoder->cam_param.pix_fmt);
    dzlog_notice("cam_params.width   = [%d]", pLocoder->cam_param.width);
    dzlog_notice("cam_params.height  = [%d]", pLocoder->cam_param.height);
    dzlog_notice("cam_params.fps     = [%d]", pLocoder->cam_param.fps);
    dzlog_notice("===============cam params end===========");
    // 计算需要申请的内存大小
    pLocoder->vm.yuv420_size = GET_YUV420_FRAME_SIZE(pLocoder->cam_param.width, pLocoder->cam_param.height);
    if(0 != InitVideoMemory(&pLocoder->vm)){
        dzlog_fatal("InitVideoMemory fail!");
        return -1;
    }
    dzlog_info("InitVideoMemory success.");
    if(0 != InitFFmpegEncoder(&pLocoder->coder)){
        dzlog_fatal("InitVideoEncoder failed!");
        return -1;
    }
    dzlog_info("InitFFmpegEncoder success.");
    pLocoder->rtmp_cb.push_url = RPI_PUSH_URL;
    pLocoder->rtmp_cb.HowToConnectServer = SrsRtmpConnect;
    pLocoder->rtmp_cb.HowToPushH264      = SrsRtmpSendH264;
    pLocoder->rtmp_cb.HowToRelease       = SrsRtmpClose;
    pLocoder->rtmp_cb.HowToReconnect     = SrsRtmpReconnect;
    if(0 != RtmpPusherInit(&pLocoder->rtmp_cb)){
        dzlog_fatal("RtmpPusherInit fail.");
        return -1;
    }
    SemBinaryP(VIDEO_PARAM_SEM_KEY, BLOCK_WAIT);//上锁
    CamParams_t cam_param;
    memset(cam_param.stream_url, 0, sizeof(cam_param.stream_url));
    ReadFromShm(VIDEO_PARAM_SHM_KEY, &cam_param, sizeof(cam_param));
    memcpy(cam_param.stream_url, RPI_PUSH_URL, strlen(RPI_PUSH_URL));
    if(SHM_OK != WriteToShm(VIDEO_PARAM_SHM_KEY, &cam_param, sizeof(cam_param))){//将视频参数更新到共享内存
        dzlog_error("WriteToShm fail, shm key: [%#x]", VIDEO_PARAM_SHM_KEY);
        SemBinaryV(VIDEO_PARAM_SEM_KEY);//解锁
        return -1;
    }
    SemBinaryV(VIDEO_PARAM_SEM_KEY);//解锁
    dzlog_info("RtmpPusherInit success.");

    dzlog_info("ProcessInit success.");
    return 0;
}

static int ProcessExit(LocoderProcess_t *pLocoder)
{
    DestroyVideoMemory(&pLocoder->vm);
    ReleaseFFmepgEncoder(&pLocoder->coder);

    return 0;
}
// 追加写入文件
static void AppendWriteToFile(const char *p_filename, char *p_buf, unsigned int size)
{
    int fd = -1;
    if(0 > (fd = open(p_filename, O_CREAT | O_WRONLY | O_APPEND, 0777))){
        perror("open()");
        return;
    }
    if(size != write(fd, p_buf, size)){
        perror("write()");
        close(fd);
        return;
    }
    close(fd);
}