#ifndef FMT_CONV_H
#define FMT_CONV_H
#include <stdio.h>
#include <stdlib.h>

// 计算YUYV帧大小
#define     GET_YUYV_FRAME_SIZE(width, height)      (2ul*(width)*(height))
// 计算YUV420帧大小
#define     GET_YUV420_FRAME_SIZE(width, height)      (3ul*(width)*(height)/2ul)

typedef enum{
    CONV_OK   =  0,//转换成功
    CONV_FAIL = -1,//转换失败
}ConvResult_t;

ConvResult_t YUYVConv2YUV420p(int width,int hight,char *yuy2,char *yuv420);
ConvResult_t YUV420pConv2YUV420sp(unsigned char* yuv420p, unsigned char* yuv420sp, int width, int height);
ConvResult_t YUV420spConv2YUV420p(unsigned char* yuv420sp, unsigned char* yuv420p, int width, int height);
#endif
