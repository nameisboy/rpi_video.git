#ifndef RTMP_PUSHER_H
#define RTMP_PUSHER_H
#include "rpv_com.h"
// #include "srsRtmp.h"

// 推流url
#define     RPI_PUSH_URL    "rtmp://120.79.54.142:1935/live/rpi"
// #define     RTMP_URL    "rtmp://120.79.54.142:1935/live/rpi"
// #define     RTMP_URL    "rtmp://127.0.0.1:8080/live/rpi"

typedef struct __RtmpH264Stream{
    char *pH264_data;
    int   data_size;
}RtmpH264Stream_t;

typedef int (*PushH264StreamFunc)(RtmpH264Stream_t *pH264Stream);
typedef int (*ConnectFunc)(const char *pUrl);
typedef int (*ReconnectFunc)(int pushState);
typedef int (*ReleaseFunc)(void *args);

typedef struct __RtmpPusherCb{
    const char *push_url;
    PushH264StreamFunc  HowToPushH264;
    ConnectFunc         HowToConnectServer;
    ReconnectFunc       HowToReconnect;
    ReleaseFunc         HowToRelease;
}RtmpPusherCb_t;

// 函数声明
int RtmpPusherInit(RtmpPusherCb_t *pRtmp);
int RtmpPusherRun(RtmpPusherCb_t *pRtmp, RtmpH264Stream_t *pH264);
#endif // !RTMP_PUSHER_H