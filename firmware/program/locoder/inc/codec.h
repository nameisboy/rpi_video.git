#ifndef CODEC_H
#define CODEC_H
#include "rpv_com.h"
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>

typedef struct __codec
{
    AVCodecContext *pEncCtx;
    AVPacket *pAvPkt;
    AVCodec *pCodec;
    AVFrame *pAvFrame;
}Codec_t;

// 编码器名称
// #define CODEC_NAME  "libx264"
#define CODEC_NAME  "h264_omx" //h264硬件编码器
// 帧高宽
#define YUV_WIDTH   640
#define YUV_HEIGHT  480

// 函数声明
int InitFFmpegEncoder(Codec_t *pEncoder);
int Yuv420pEncodeToH264(Codec_t *pCodec, char *pYuvbuf, unsigned char *pH264buf, int *pH264size);
int ReleaseFFmepgEncoder(Codec_t *pCodec);
#endif // !CODEC_H