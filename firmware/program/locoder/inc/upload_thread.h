#ifndef UPLOAD_THREAD_H
#define UPLOAD_THREAD_H
#include <string.h>
#include "rpv_com.h"
#include "codec.h"
#include "rtmp_pusher.h"

#define     CONV_FMT_DEBUG      0

#define     CONV_DEBUG_FILE     "/rpv/log/conv_debug"
#define     YUV420_FILE_PATH    "/rpv/log/video/yuv420.yuv"

/**
 * @brief 视频相关内存
 * 
 */
typedef struct __VideoMemory{
    char *pYuv420;
    unsigned int yuv420_size;
}VideoMemory_t;
/**
 * @brief locoder进程结构体
 * 
 */
typedef struct __LocoderProcess{
    Codec_t    coder;//编码器
    VideoMemory_t vm;//内存
    CamParams_t cam_param;//摄像头参数
    RtmpPusherCb_t rtmp_cb;//rtmp推流器
}LocoderProcess_t;
void *upload_thread(void *args);
#endif // !UPLOAD_THREAD_H