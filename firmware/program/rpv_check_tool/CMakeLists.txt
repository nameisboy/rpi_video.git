cmake_minimum_required(VERSION 3.0.0)
# 设置编译工具链
set(CMAKE_C_COMPLIER ${GCC})
set(CMAKE_CXX_COMPLIER ${GXX})
# 工程名
project(rpv_check_tool VERSION 0.1.0)

# 设置头文件查找路径
include_directories(
    ${PROJECT_SOURCE_DIR}/inc
    ${RPVCOM_INC}
)
# 查找src目录下的源文件，赋值到SRCS变量
aux_source_directory(${PROJECT_SOURCE_DIR}/src SRCS)
aux_source_directory(${RPVCOM_SRC} SRCS)
# 设置库文件搜索路径
link_directories(
    "${LIBSO_DIR}/"
    "${LIBA_DIR}/"
)
# 生成可执行文件
add_executable(${PROJECT_NAME} ${SRCS})

# make install时安装库到指定路径
target_link_libraries(
    ${PROJECT_NAME}
    Cipc
    zlog
    pthread
)
# make install时复制到指定路径
install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${BIN_PATH})