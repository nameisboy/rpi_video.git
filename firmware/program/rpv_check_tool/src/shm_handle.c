#include "shm_handle.h"

void PrintVideoFrameShm(void)
{
    VideoFrame_t frame;
    ReadFromShm(VIDEO_FRAME_SHM_KEY, &frame, sizeof(frame));
    printf("============VideoFrame_t Info============\n");
    printf("Shm-key:    [%#x]\n", VIDEO_FRAME_SHM_KEY);
    printf("frame_size: [%d]\n", frame.frame_size);
    printf("============VideoFrame_t Info End========\n");
}

void PrintCamParamsShm(void)
{
    CamParams_t cam_params;
    ReadFromShm(VIDEO_PARAM_SHM_KEY, &cam_params, sizeof(cam_params));
    printf("============CamParams_t Info============\n");
    printf("Shm-key: [%#x]\n", VIDEO_PARAM_SHM_KEY);
    printf("pix_fmt: [%#x]\n", cam_params.pix_fmt);
    printf("width:   [%d]\n", cam_params.width);
    printf("height:  [%d]\n", cam_params.height);
    printf("fps:     [%d]\n", cam_params.fps);
    printf("============CamParams_t Info End========\n");
}

/**
 * @brief 根据ShmId读取共享内存打印相关信息
 * 
 * @param shm_id 
 */
int ReadShmById(ShmId_t shm_id)
{
    switch (shm_id){
        case VIDEO_FRAME:
            PrintVideoFrameShm();
            break;
        case VIDEO_PARAM:
            PrintCamParamsShm();
            break;
        default:
            return -1;
            break;
    }
    return 0;
}