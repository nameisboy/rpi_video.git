#include "rpv_com.h"
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>
#include "shm_handle.h"

// 默认保存路径
#define DEFAULT_SAVE_PATH   "/rpv/log/video/"

typedef struct {
    unsigned int number;//帧数
    unsigned int interval;//读取数据间隔
    char filename[100];//保存路径
    signed char shm_id;//需要查询的共享内存对应的ID
}CmdArgs_t;
// 函数声明
static void PrintUsage(void);
static void PrintShmId(void);
void HandleCmdArgs(int argc, char *argv[], CmdArgs_t *p_cmd_args);
static int GeneraFileNameByTime(char *p_suffix, char *filename_ptr);
static int AppendWriteToFile(const char *path_ptr, void* data_ptr, unsigned int data_len);
int main(int argc, char *argv[])
{
    VideoFrame_t video_frame;
    CamParams_t cam_params;
    CmdArgs_t cmd_args;
    char file_path_buf[200] = {0};
    memset(&cmd_args, 0, sizeof(cmd_args));
    cmd_args.interval = 10;//默认参数10ms间隔
    cmd_args.number   = 0;//一直获取
    cmd_args.shm_id = -1;
    if(argc > 1)
        HandleCmdArgs(argc, argv, &cmd_args);//处理命令行参数
    else{
        PrintUsage();
        exit(0);
    }
    if(cmd_args.shm_id >= 0){//检查是否需要读取共享内存
        printf("cmd_args.shm_id = %d\r\n", cmd_args.shm_id);
        if(0 != ReadShmById(cmd_args.shm_id)){
            PrintShmId();
            exit(-1);
        }
        exit(0);//使用-m参数直接退出此程序
    }
    memset(file_path_buf, 0, sizeof(file_path_buf));
    if(0 == strlen(cmd_args.filename)){//未获取到文件名
        if(0 != GeneraFileNameByTime(".yuv", cmd_args.filename)){//生成文件名   
            return -1;
        }
        printf("No file name entered, system genera filename: %s\n", cmd_args.filename);
        snprintf(file_path_buf, sizeof(file_path_buf), "%s%s", DEFAULT_SAVE_PATH, cmd_args.filename);
        printf("save file path: %s\n", file_path_buf);
    }else{
        memcpy(file_path_buf, cmd_args.filename, strlen(cmd_args.filename));
        printf("save file path: %s\n", file_path_buf);
    }
    while(1)
    {
        memset(&video_frame, 0, sizeof(VideoFrame_t));
        if(SHM_OK != ReadFromShm(VIDEO_FRAME_SHM_KEY, &video_frame, sizeof(VideoFrame_t))){
            printf("ReadFromShm fail!\n");
            exit(-1);
        }
        if(0 != AppendWriteToFile(file_path_buf, video_frame.video_frame_buf, video_frame.frame_size)){
            printf("AppendWriteToFile fail!\n");
            exit(-1);
        }
        usleep(cmd_args.interval*1000);
        if(0 < cmd_args.number){
            cmd_args.number--;
            if(0 == cmd_args.number){
                printf("capture finish!\n");
                break;
            }
        }
    }
    
    exit(0);
}

static const char *p_short_opt_string = "n:i:f:m:hv";//短选项
static struct option long_opts[] = {//长选项
    {"number", required_argument, NULL, 'n'},//帧数，带参，需要读取的帧数量，未指定则为无穷(-1)
    {"interval", required_argument, NULL, 'i'},//采样时间，带参, 单位ms，不设置则默认为一直读取
    {"filename", required_argument, NULL, 'f'},//生成文件保存路径，带参，默认使用年月日时分秒文件名，默认路径/rpv/log/video/xxx
    {"memory", required_argument, NULL, 'm'},//共享内吨对应的ID
    {"help", no_argument, NULL, 'h' },//help不需要参数
    { 0, 0, 0, 0 }//最后一个必须为空
};
/**
 * @brief 处理命令行参数，处理后的将数据填充到p_cmd_args
 * 
 * @param argc 命令行参数个数 
 * @param argv 命令行参数列表
 * @param p_cmd_args 回填参数
 */
void HandleCmdArgs(int argc, char *argv[], CmdArgs_t *p_cmd_args)
{
    int retval = -1;
    while(-1 != (retval = getopt_long(argc, argv, p_short_opt_string, long_opts, NULL))){
        switch (retval){
            case 'n':
                p_cmd_args->number = atoi(optarg);
                break;
            case 'i':// @Todo此处最好限制输入的参数大小
                p_cmd_args->interval = atoi(optarg);
                break;
            case 'f':
                memset(p_cmd_args->filename, 0, sizeof(p_cmd_args->filename));
                memcpy(p_cmd_args->filename, optarg, strlen(optarg));
                break;
            case 'm':
                printf("m.\n");
                if(NULL != optarg){
                    p_cmd_args->shm_id = atoi(optarg);
                }else{
                    PrintShmId();
                }
                break;
            case 'v':
                printf("verison: V1.0\n");
            case 'h':            
            default:
                PrintUsage();
                exit(0);//查看help和版本信息，直接退出进程
                break;
        }
    }
}
/**
 * @brief 打印使用方法
 * 
 */
static void PrintUsage(void)
{
    printf("rpv_check_tool(V1.0) was built at [%s %s]\n", __DATE__, __TIME__);
    printf("option: \n");
    printf("\t--number or -n: enter how many frame to capture.\n");
    printf("\t--interval or -i: enter the interval to capture.\n");
    printf("\t--filename or -f: enter the filename to save, default path & name is: %sYYMMDD_hhmmss.yuv\n",
            DEFAULT_SAVE_PATH);
    printf("\t--version or -v: print the version of the tool\n");
    printf("\t--help or -h: print the help info of the tool\n");
}
/**
 * @brief 打印共享内存对应id
 * 
 */
static void PrintShmId(void)
{
    printf("opt<-m> usage: \n");
    printf("\t SHM_ID 0: VIDEO_FRAME\n");
    printf("\t SHM_ID 1: VIDEO_PARAM\n");
    printf("example: rpv_check_tool -m 0\n");
}
/**
 * @brief 根据当前时间生成文件名, YYMMDD_hhmmss格式
 * 
 * @return int 返回0成功，否则失败.
 */

/**
 * @brief 根据当前时间生成文件名, YYMMDD_hhmmss格式
 * 
 * @param p_suffix 生成的文件名的后缀
 * @param filename_ptr 指向生成后的文件名，不能为NULL
 * @return int 成功返回0，否则失败.
 */
static int GeneraFileNameByTime(char *p_suffix, char *filename_ptr)
{   
    char tmp_buf[100] = {0};
    int name_len = 13;//初始长度，YYMMDD_hhmmss
    time_t timestamp = time(NULL);
    struct tm *p_datetime = NULL;
    p_datetime = localtime(&timestamp);
    memset(tmp_buf, 0, sizeof(tmp_buf));
    if(0 > (snprintf(tmp_buf, sizeof(tmp_buf), "%02d%02d%02d_%02d%02d%02d",
            p_datetime->tm_year+1900,
            p_datetime->tm_mon+1,
            p_datetime->tm_mday,
            p_datetime->tm_hour,
            p_datetime->tm_min,
            p_datetime->tm_sec))){
        printf("snprintf err!\n");
        return -1;
    }
    strcat(tmp_buf, p_suffix);
    memcpy(filename_ptr, tmp_buf, strlen(tmp_buf));

    return 0;
}
/**
 * @brief 追加写入文件
 * 
 * @param path_ptr 文件路径指针
 * @param data_ptr 数据指针
 * @param data_len 数据长度
 * @return int 返回0成功，否则失败
 */
static int AppendWriteToFile(const char *path_ptr, void* data_ptr, unsigned int data_len)
{
    int fd = -1;
    if(0 > (fd = open(path_ptr, O_CREAT | O_WRONLY | O_APPEND, 0777))){
        perror("open()");
        return -1;
    }
    if(data_len != write(fd, data_ptr, data_len)){
        perror("write()");
        close(fd);
        return -1;
    }
    close(fd);
    return 0;
}