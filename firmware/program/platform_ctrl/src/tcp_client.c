#include "rpv_com.h"
#include "tcp_client.h"
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

static volatile int tcp_socket = -1;
int TcpClientInit(const char *server_addr, const unsigned int port)
{
    int sock;
    struct sockaddr_in servAddr;
    memset(&servAddr, 0, sizeof(servAddr));
    //创建socket套接字
    if (0 > (sock = socket(AF_INET, SOCK_STREAM, 0)))
    {
        dzlog_error("Tcp socket create fail: %s.", strerror(errno));
        return -1;
    }
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr(server_addr);
    servAddr.sin_port = htons(port);
    if (0 != connect(sock, (struct sockaddr *)&servAddr, sizeof(servAddr)))
    {
        dzlog_info("Tcp client init successs, connect [%s]:[%d] fail!", server_addr, port);
        close(sock);
        return -1;
    }
    dzlog_info("Tcp client init successs, connect [%s]:[%d] success!", server_addr, port);
    tcp_socket = sock;
    return 0;
}
static int CheckSocketValid(int sock_fd)
{
    if (sock_fd < 2)
    {
        dzlog_fatal("sock fd[%d] invalid.", sock_fd);
        return -1;
    }
    return 0;
}
int TcpSendData(char *s_buf, int s_size)
{
    if (0 != CheckSocketValid(tcp_socket)){
        return -1;
    }
    fd_set w_fdset;
    FD_ZERO(&w_fdset);
    FD_SET(tcp_socket, &w_fdset);
    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0; // 100ms
    int ret = select(tcp_socket+1, NULL, &w_fdset, NULL, &tv);
    if (ret < 0)
    {
        dzlog_error("select error: %s.", strerror(errno));
        return -1;
    }
    else if (ret == 0)
    {
        dzlog_error("select timeout.");
        return -1;
    }
    else
    {
        if (FD_ISSET(tcp_socket, &w_fdset))
        {
            if (s_size != write(tcp_socket, s_buf, s_size))
            {
                dzlog_error("write s_buf fail, size: [%d].", s_size);
                return -1;
            }
        }
    }

    return 0;
}
int TcpRecvData(void *r_buf, int buf_size)
{
    if (0 != CheckSocketValid(tcp_socket)){
        return -1;
    }
    fd_set r_fdset;
    FD_ZERO(&r_fdset);
    FD_SET(tcp_socket, &r_fdset);
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100 * 1000; // 100ms
    int ret = select(tcp_socket+1, NULL, &r_fdset, NULL, &tv);
    if (ret < 0){
        dzlog_error("select error: %s.", strerror(errno));
        return -1;
    }else if (ret == 0){
        // dzlog_notice("select timeout.");
        return -1;
    }else{
        if(FD_ISSET(tcp_socket, &r_fdset)){
            return read(tcp_socket, r_buf, buf_size);
        }
    }
    return 0;
}
int TcpClientDestroy(void)
{
    if (2 < tcp_socket){
        close(tcp_socket);
        tcp_socket = -1;
    }else{
        dzlog_warn("tcp_socket val: [%d] error.", tcp_socket);
        return -1;
    }

    return 0;
}

#define     HEARTBEAT_CONTENT   "1"
/**
 * @brief 发送心跳包函数
 * 
 * @return int 
 */
int TcpHeartbeat(void)
{
    if (0 != CheckSocketValid(tcp_socket)){
        dzlog_error("Tcp client socket(%d) error.", tcp_socket);
        return -1;
    }
    return TcpSendData(HEARTBEAT_CONTENT, strlen(HEARTBEAT_CONTENT));
}
