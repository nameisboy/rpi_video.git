#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "rpv_com.h"
#include "servo.h"
#include "tcp_client.h"
#include "platform_common.h"
#include "tcp_fifo.h"

static void InitZlog(void)
{
	int ret = dzlog_init(ZLOG_CONF_FILEPATH, ZLOG_CATEGORY_PLF_CTRL);
	if(0 != ret){
		dzlog_error("dzlog_init fail, ret: [%d]\n", ret);
	}
}
#define		DEV_ID	"10000001"//设备ID,目前先写死
static int SendOnlineMsg(void)
{
	SemBinaryP(VIDEO_PARAM_SEM_KEY, BLOCK_WAIT);//上锁
		CamParams_t cam_param;
		memset(&cam_param, 0, sizeof(cam_param));
		ReadFromShm(VIDEO_PARAM_SHM_KEY, &cam_param, sizeof(CamParams_t));
	SemBinaryV(VIDEO_PARAM_SEM_KEY);
	char online_msg_buf[200] = {0};
	memset(online_msg_buf, 0, sizeof(online_msg_buf));
	snprintf(online_msg_buf, sizeof(online_msg_buf), "online#%s#%s#", DEV_ID, cam_param.stream_url);
	if(0 != TcpSendData(online_msg_buf, strlen(online_msg_buf))){
		dzlog_error("Send online fail.");
		return -1;
	}
	return 0;
}
static int CreateThreads(void *arg)
{
	pthread_t thread_id = -1;
	if(0 != pthread_create(&thread_id, NULL, ParseThread, arg)){
		dzlog_fatal("Create ParseThread fail");
		return -1;
	}
	pthread_t heartbeat_thread_id = -1;
	if(0 != pthread_create(&heartbeat_thread_id, NULL, TcpHeartbeatThread, NULL)){
		dzlog_fatal("Create ParseThread fail");
		return -1;
	}
	return 0;
}
int ProgramInit(void* arg)
{
	PlatformProgramObj_t *p_obj = (PlatformProgramObj_t*)arg;
    InitZlog();
	if(0 != ServoInit()){
		return -1;
	}
	if(0 != TcpClientInit(TCP_SERVER_ADDR, TCP_SERVER_PORT)){
		return -1;
	}
	if(0 != InitTcpDataFifo(&p_obj->fd_tcp_fifo_w, &p_obj->fd_tcp_fifo_r)){
		return -1;
	}
	CreateThreads(p_obj);
	SendOnlineMsg(); //发送上线消息
	dzlog_info("ProgramInit success!");
	return 0;
}