#include "tcp_fifo.h"

#define     TCP_RCV_DATA_FIFO   "/tmp/tcp_rcv_data_fifo"
#define     WR_MAX_SIZE         4096
int InitTcpDataFifo(int *fd_w_fifo, int *fd_r_fifo)
{
    if(!fd_w_fifo || !fd_r_fifo){
        dzlog_error("param point has null.");
        return -1;
    }
    if(0 == access(TCP_RCV_DATA_FIFO, F_OK)){
        dzlog_notice("[%s] exist, now unlink it.", TCP_RCV_DATA_FIFO);
        unlink(TCP_RCV_DATA_FIFO);
    }
    if(0 != mkfifo(TCP_RCV_DATA_FIFO, 0777)){
        dzlog_error("mkfifo %s fail: %s", TCP_RCV_DATA_FIFO, strerror(errno));
        return -1;
    }
    int w_fifo_fd, r_fifo_fd;
    r_fifo_fd = open(TCP_RCV_DATA_FIFO, O_NONBLOCK | O_RDONLY);
    if(r_fifo_fd < 0){
        dzlog_error("open TCP_RCV_DATA_FIFO error: %s.", strerror(errno));
        close(w_fifo_fd);
        return -1;
    }
    w_fifo_fd = open(TCP_RCV_DATA_FIFO, O_NONBLOCK | O_WRONLY, 0666);
    if(w_fifo_fd < 0){
        dzlog_error("open [%s] error: %s.", TCP_RCV_DATA_FIFO, strerror(errno));
        return -1;
    }

    *fd_w_fifo = w_fifo_fd;
    *fd_r_fifo = r_fifo_fd;

    return 0;
}

int WriteToTcpFifo(int fd_to_write, char *p_data, int data_size)
{
    if(fd_to_write <= 2 || !p_data || data_size > WR_MAX_SIZE){
        return -1;
    }
    return write(fd_to_write, p_data, data_size);
}

int ReadFromTcpFifo(int fd_to_read, char *p_data)
{
    if(fd_to_read <= 2 || !p_data){
        return -1;
    }
    fd_set r_fdset;
    FD_ZERO(&r_fdset);
    FD_SET(fd_to_read, &r_fdset);
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100 * 1000; // 100ms
    int ret = select(fd_to_read+1, &r_fdset, NULL, NULL, &tv);
    if (ret < 0){
        dzlog_error("select error: %s.", strerror(errno));
        return -1;
    }else if (ret == 0){
        dzlog_debug("select timeout.");
        return 0;
    }else{
        if(FD_ISSET(fd_to_read, &r_fdset)){
            return read(fd_to_read, p_data, WR_MAX_SIZE);
        }
    }

}