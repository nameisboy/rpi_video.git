#include <pthread.h>
#include "platform_common.h"
#include "rpv_com.h"
#include "tcp_fifo.h"
#include "servo.h"

// 舵机控制列表
struct {
    char *ctrl_direct_matchstr;//用于匹配的字符串
    int (*CtrlHandleFunc)(int direction, int angle);
    int ctrl_dir;//控制方向
    int ctrl_angle;//控制角度
}ServoCtrlList[] = {
    {"left",    ServoTurn,  LEFT,   8},
    {"right",   ServoTurn,  RIGHT,  8},
    {"up",      ServoTurn,  UP,     8},
    {"down",    ServoTurn,  DOWN,   8},
};

#define TCP_FIFO_SIZE   (4096L)
/**
 * @brief 解析并控制舵机函数
 * 
 * @param raw_data_p 
 * @param data_size 
 * @return int 
 */
static int ParseAndCtrl(char *raw_data_p, int data_size)
{
    if(!raw_data_p){
        return -1;
    }
    char temp_buf[TCP_FIFO_SIZE] = {0};
    char line_buff[TCP_FIFO_SIZE] = {0};
    memset(temp_buf, 0, sizeof(temp_buf));
    memset(line_buff, 0, sizeof(line_buff));
    memcpy(temp_buf, raw_data_p, data_size);
    char *p_data = temp_buf;
    // 1.截取一行数据
    char *p_start = p_data;
    while(p_data < temp_buf+TCP_FIFO_SIZE){
        if(*p_data == '\r' && *(p_data+1) == '\n'){//匹配1行数据
            memset(line_buff, 0, sizeof(line_buff));
            memcpy(line_buff, p_start, p_data - p_start);
            dzlog_info("line_buff: [%s]", line_buff);
            p_start = p_data + 2;
            int i = 0;
            for(i=0; i<sizeof(ServoCtrlList)/sizeof(ServoCtrlList[0]); i++){
                if(strstr(line_buff, ServoCtrlList[i].ctrl_direct_matchstr)){
                    ServoCtrlList[i].CtrlHandleFunc(ServoCtrlList[i].ctrl_dir, ServoCtrlList[i].ctrl_angle);
                }
            }
        }
        p_data++;
    }

    // 2.解出控制方向，调用对应函数完成控制

}
void *ParseThread(void *args)
{
    PlatformProgramObj_t *p_obj = (PlatformProgramObj_t*)args;
    char recv_buf[TCP_FIFO_SIZE] = {0};
    memset(recv_buf, 0, sizeof(recv_buf));
    int read_size = -1;
    while(1){
        if(0 < (read_size = ReadFromTcpFifo(p_obj->fd_tcp_fifo_r, recv_buf))){
            dzlog_info("Read from tcp fifo: [%s].", recv_buf);
            ParseAndCtrl(recv_buf, read_size);
        }
    }
    pthread_exit(0);
}