#include <pthread.h>
#include <unistd.h>
#include "rpv_com.h"
#include "tcp_client.h"

#define     HEARTBEAT_PERIOD    (300L)
void *TcpHeartbeatThread(void *args)
{
    int cnt = 0;
    while(1){
        usleep(100*1000);
        cnt++;
        if(cnt >= HEARTBEAT_PERIOD){//三十秒发一次心跳包
            cnt = 0;
            if(0 != TcpHeartbeat()){
                dzlog_warn("TcpHeartbeat fail.");
            }
        }
    }
    pthread_exit(0);
}