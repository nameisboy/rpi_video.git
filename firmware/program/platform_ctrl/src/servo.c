#include "pwm_ctrl.h"
#include "servo.h"

static PwmConfig_t servo1_pwm_config = {
    .pwm_chip_num = SERVO0_PWM_CHIP,
    .pwm_channel_num = SERVO0_PWM_CHANNE,
    .polarity = SERVO0_PWM_POLARITY, //极性
    .period = SERVO_PWM_PERIOD,      // pwm周期
    .duty_cycle = 0,                 //占空比
};
static PwmConfig_t servo2_pwm_config = {
    .pwm_chip_num = SERVO1_PWM_CHIP,
    .pwm_channel_num = SERVO1_PWM_CHANNE,
    .polarity = SERVO1_PWM_POLARITY, //极性
    .period = SERVO_PWM_PERIOD,      // pwm周期
    .duty_cycle = 0,                 //占空比
};

/**
 * @brief 复位云台位置
 *
 * @return int
 *  返回0成功，否则失败
 */
static int PlatformResetPosition(void)
{
    if (0 != PwmSetDutyCycle(&servo1_pwm_config, SG90_90_DEG_DUTY_CYCLE))
    {
        dzlog_error("PlatformResetPosition fail.");
        return -1;
    }
    if (0 != PwmSetDutyCycle(&servo2_pwm_config, SG90_90_DEG_DUTY_CYCLE))
    {
        dzlog_error("PlatformResetPosition fail.");
        return -1;
    }
    return 0;
}
/**
 * @brief 舵机初始化
 *
 * @return int
 *  返回0成功，否则失败
 */
int ServoInit(void)
{
    do
    {
        if (0 != PwmOpen(servo1_pwm_config))
        {
            dzlog_error("servo1 pwm init fail.");
            break;
        }
        if (0 != PwmOpen(servo2_pwm_config))
        {
            dzlog_error("servo2 pwm init fail.");
            break;
        }
        if (0 != PlatformResetPosition())
        {
            break;
        }
        return 0;
    } while (0);

    return -1;
}

/**
 * @brief 角度转舵机控制对应的微秒数
 *
 * @param angle
 * @return int
 */
static double AngleToUs(int angle)
{
    return (angle * US_PER_DEG);
}
int ServoTurn(int direction, int angle)
{
    double duty_cycle_us = AngleToUs(angle);
    PwmConfig_t *pwm;
    double duty_to_set = 0.0;
    switch (direction)
    {
        case LEFT:
            pwm = &servo1_pwm_config;
            duty_to_set = pwm->duty_cycle + duty_cycle_us;
            break;
        case RIGHT:
            pwm = &servo1_pwm_config;
            duty_to_set = pwm->duty_cycle - duty_cycle_us;
            break;
        case UP:
            pwm = &servo2_pwm_config;
            duty_to_set = pwm->duty_cycle - duty_cycle_us;
            break;
        case DOWN:
            pwm = &servo2_pwm_config;
            duty_to_set = pwm->duty_cycle + duty_cycle_us;
            break;
        default:
            dzlog_error("Unkonw direction: [%d].", direction);
            return -1;
            break;
    }
    dzlog_info("duty_to_set: [%.3lf].", duty_to_set);
    // 限制占空比，超出范围舵机会异常转动
    if(duty_to_set > SERVO_MAX_DUTY_CYCLE || duty_to_set < SERVO_MIN_DUTY_CYCLE){
        dzlog_fatal("duty_cycle_us(%.3lf) error.", duty_to_set);
        return -1;
    }
    double percent = duty_to_set / pwm->period;
    percent *= 100.0;
    if(0 != PwmSetDutyCycle(pwm, percent)){
        dzlog_error("PwmSetDutyCycle fail");
        return -1;
    }
    return 0;
}

// int ServoTurnRight(int angle)
// {
//     double duty_cycle_us = AngleToUs(angle);
//     // 限制占空比，超出范围舵机会异常转动
//     if(duty_cycle_us > SERVO_MAX_DUTY_CYCLE || duty_cycle_us < SERVO_MIN_DUTY_CYCLE){
//         dzlog_fatal("caculate angle(%d), duty_cycle_us(%.3lf) error.", angle, duty_cycle_us);
//         return -1;
//     }
//     double percent = (servo1_pwm_config.duty_cycle - duty_cycle_us) / servo1_pwm_config.period;
//     percent *= 100.0;
//     if(0 != PwmSetDutyCycle(&servo1_pwm_config, percent)){
//         dzlog_fatal("PwmSetDutyCycle fail");
//         return -1;
//     }
//     return 0;
// }

// int ServoTurnUp(int angle)
// {
// }

// int ServoTurnDown(int angle)
// {
// }