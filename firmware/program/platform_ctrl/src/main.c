#include "platform_common.h"
#include "tcp_fifo.h"

static PlatformProgramObj_t platform_obj;
int main(int argc, char *argv[])
{
    if(0 != ProgramInit(&platform_obj)){
		dzlog_fatal("Program init fail.");
		exit(-1);
	}
	char tcp_buffer[1024] = {0};
	memset(tcp_buffer, 0, sizeof(tcp_buffer));

	while(1){
		int ret = TcpRecvData(tcp_buffer, sizeof(tcp_buffer));
		if(ret > 0){
			dzlog_info("Tcp recv: [%s].", tcp_buffer);
			if(0 > (WriteToTcpFifo(platform_obj.fd_tcp_fifo_w, tcp_buffer, ret))){
				dzlog_error("Write tcp fifo error.");
			}
			memset(tcp_buffer, 0, sizeof(tcp_buffer));
		}
		// usleep(10*1000);
	}
    return 0;
}
