#ifndef __TCP_CLIENT_H
#define __TCP_CLIENT_H
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define     TCP_SERVER_PORT             (8080)
#define     TCP_SERVER_ADDR             "114.132.79.49"

int TcpClientInit(const char *server_addr, const unsigned int port);
int TcpSendData(char *s_buf, int s_size);
int TcpRecvData(void *r_buf, int buf_size);
int TcpClientDestroy(void);
int TcpHeartbeat(void);
#endif // !__TCP_CLIENT_H