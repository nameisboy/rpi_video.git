#ifndef TCP_FIFO_HEADER
#define TCP_FIFO_HEADER
#include <limits.h>
#include "rpv_com.h"
#include <sys/select.h>

int InitTcpDataFifo(int *fd_w_fifo, int *fd_r_fifo);
int WriteToTcpFifo(int fd_to_write, char *p_data, int data_size);
int ReadFromTcpFifo(int fd_to_read, char *p_data);
#endif