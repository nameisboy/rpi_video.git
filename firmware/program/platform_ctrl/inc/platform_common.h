#ifndef __PLATFORM_COMMON_H
#define __PLATFORM_COMMON_H
#include "rpv_com.h"
#include "tcp_client.h"


typedef struct PlatformProgramObj_st{
    int fd_tcp_fifo_w;
    int fd_tcp_fifo_r;
    
}PlatformProgramObj_t;
void *ParseThread(void *args);
int ProgramInit(void* arg);
void *TcpHeartbeatThread(void *args);
#endif // !__PLATFORM_COMMON_H