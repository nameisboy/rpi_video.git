#ifndef __SERVO_H
#define __SERVO_H
#include <stdio.h>
#include <stdlib.h>

#define     SERVO0_PWM_CHIP   0
#define     SERVO1_PWM_CHIP   0

#define     SERVO0_PWM_CHANNE     0
#define     SERVO1_PWM_CHANNE     1

#define     SERVO0_PWM_POLARITY     NORMAL
#define     SERVO1_PWM_POLARITY     NORMAL

#define     SERVO_PWM_PERIOD        (20UL*1000*1000)
// 90°角度对应的占空比
// 1.5ms/20ms * 100 = 7.5
#define     SG90_90_DEG_DUTY_CYCLE   (7.5)
#define     MAX_ANGLE_LIMIT          (90)
#define     US_PER_DEG               ((2.0*1000.0*1000.0)/180.0)
// 最大占空比ms
#define     SERVO_MAX_DUTY_CYCLE     (2.5*1000.0*1000.0)
// 最小占空比ms
#define     SERVO_MIN_DUTY_CYCLE     (0.5*1000.0*1000.0)

enum TurnDirection{
    LEFT  = 1,
    RIGHT = 2,
    UP    = 3,
    DOWN  = 4,
};
// 外部接口函数
int ServoInit(void);
int ServoTurn(int direction, int angle);
#endif // !__SERVO_H