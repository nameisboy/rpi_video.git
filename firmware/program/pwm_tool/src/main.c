#include <stdio.h>
#include <stdlib.h>
#include "pwm_ctrl.h"

#define     SERVO_PWM_PERIOD_US   (20UL*1000*1000)  //20ms

int main(int argc, char *argv)
{
    PwmConfig_t pwm_config = {
        .pwm_chip_num = 0,
        .pwm_channel_num = 0,
        .polarity = NORMAL,//极性
        .period = SERVO_PWM_PERIOD_US,//pwm周期
        .duty_cycle = 0,//占空比
    };
    if(0 != PwmOpen(pwm_config)){
        printf("Pwm Open fail.\r\n");
        exit(-1);
    }
    double duty_cycle_set = 0;
    printf("Set duty_cycle:");
    scanf("%lf", &duty_cycle_set); 
    printf("duty_cycle will set: [%.1lf]\n", duty_cycle_set);
    if(0 != PwmSetDutyCycle(&pwm_config, duty_cycle_set)){
        printf("Set [%.1lf] duty_cycle fail.\r\n", duty_cycle_set);
    }

    return 0;
}